// include gulp
var gulp = require('gulp'),

    // include plug-ins
    clean = require('gulp-clean'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    changed = require('gulp-changed'),
    imagemin = require('gulp-imagemin'),
    dust = require('gulp-dust'),
    concat = require('gulp-concat'),
    stripDebug = require('gulp-strip-debug'),
    uglify = require('gulp-uglify'),
    autoprefix = require('gulp-autoprefixer'),
    minifyCSS = require('gulp-minify-css'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    mocha = require('gulp-mocha'),
    phantom = require("gulp-phantom"),
    spawn = require('child_process').spawn,
    gutil = require('gulp-util'),
    watch = require('gulp-watch'),
    browserify = require('gulp-browserify'),
    rjs = require("gulp-rjs"),



    bases = {
        app: './public/',
        dist: './dist/',
    },

    paths = {
        scripts: ['scripts/assets/js/**/*.js'],
        libs: ['scripts/libs/jquery/dist/jquery.js', 'scripts/libs/underscore/underscore.js', 'scripts/backbone/backbone.js'],
        styles: ['styles/**/*.css'],
        sass: ['styles/scss/*.scss'],
        html: ['index.html', '404.html'],
        images: ['images/**/*.png'],
        extras: ['crossdomain.xml', 'humans.txt', 'manifest.appcache', 'robots.txt', 'favicon.ico'],
    };

// Delete the dist directory
gulp.task('clean', function() {
    return gulp.src(bases.dist)
        .pipe(clean());
});

// JS hint task
gulp.task('jshint', function() {
    gulp.src(['./**/*.js', '!./node_modules/**/*.js', '!./**/*.min.js', '!./public/assets/bower_components/**/*.js',
        '!app.build.js', '!main-built.js', '!public/assets/templates/compiled/dust-runtime.js', '!./dist/**/*.js'
    ])
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

gulp.task('addRequire', function(){
    gulp.src('./public/assets/bower_components/requirejs/require')
    .pipe(gulp.dest('./dist/assets/scripts'));
});

// gulp.task('r', function() {
//     gulp.src(bases.app + paths.scripts)
//         .pipe(gulp.dest(bases.dist))
//         .pipe(rjs({
//         //appDir: './public',
//         mainConfigFile: "./public/assets/js/config.js",
//         baseUrl: "dist/assets/js",
//         //appDir: "./",
//         locale: "en-gb",
//         fileExclusionRegExp: /^\./,
//         removeCombined: false,
//         findNestedDependencies: true,
//         dir: "./",
//         optimize: "uglify2",
//         preserveLicenseComments: false,
//         // modules: [{
//         //     name: "config",
//         //     exclude: [
//         //         "infrastructure"
//         //     ]
//         // }, {
//         //     name: "infrastructure"
//         // }],
//         // paths: {
//         //     jquery: "empty:" //for cdns
//         // },
//         generateSourceMaps: true
//     }));
// });


// minify new images
gulp.task('imagemin', function() {
    var imgSrc = './public/assets/images/**/*',
        imgDst = './dist/assets/images';

    gulp.src(imgSrc)
        .pipe(changed(imgDst))
        .pipe(imagemin())
        .pipe(gulp.dest(imgDst));
});


gulp.task('dustb', function() {

    gulp.src('./templates/*.dust')
        .pipe(dust())
        .pipe(gulp.dest('./templates/compiled'));
});

gulp.task('dustf', function() {

    gulp.src('./public/assets/templates/*.dust')
        .pipe(dust())
        .pipe(concat('templates.js'))
        .pipe(gulp.dest('./public/assets/templates/compiled/'))
        .pipe(gulp.dest('./dist/assets/templates/compiled/'));
});


// JS concat, strip debugging and minify
gulp.task('scripts', function() {
    gulp.src(['./src/scripts/lib.js', './src/scripts/*.js'])
        .pipe(concat('script.js'))
        .pipe(stripDebug())
        .pipe(uglify())
        .pipe(gulp.dest('./build/scripts/'));
});

//Browerify
gulp.task('browserify', function() {
    gulp.src(['./public/assets/js/helpers/custom_dust_helpers/helper-dateFormat.js'])
        .pipe(browserify())
        .pipe(gulp.dest('./public/assets/js/helpers'));
});


// CSS concat, auto-prefix and minify
gulp.task('styles', function() {
    gulp.src([
        './public/assets/bower_components/bootstrap/dist/css/bootstrap.css',
        './public/assets/bower_components/bootstrap/dist/css/bootstrap-theme.css',
        './public/assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.css',
        './public/assets/bower_components/jquery-ui/themes/ui-lightness/jquery-ui.css',
        './public/assets/styles/css/styles.css'
    ])
        .pipe(concat('styles-built.min.css'))
        .pipe(minifyCSS({
            noAdvanced: true
        }))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest('./dist/assets/styles/css/'));
});


gulp.task('sass', function() {
    gulp.src('./public/assets/styles/scss/*.scss')
        .pipe(sass({
            errLogToConsole: true
        }))
        .pipe(gulp.dest('./public/assets/styles/css'))
        .pipe(gulp.dest('./dist/assets/styles/css'));
});


gulp.task('mocha', function() {
    return gulp.src('./tests/unit_tests/*.js', {
            read: false
        })
        .pipe(mocha({
            reporter: 'nyan'
        }));
});


gulp.task('phantom', function() {
    gulp.src("./phantom/*.js")
        .pipe(phantom({
            ext: '.json'
        }))
        .pipe(gulp.dest("./data/"));
});


gulp.task('casper', function() {
    var tests = './tests/functional_tests/*.js';

    var casperChild = spawn('casperjs', ['casper'].concat(tests));

    casperChild.stdout.on('data', function(data) {
        gutil.log('CasperJS:', data.toString().slice(0, -1)); // Remove \n
    });

    casperChild.on('close', function(code) {
        var success = code === 0; // Will be 1 in the event of failure

        // Do something with success here
    });
});

gulp.task('watch', function() {
    var styles = ['sass', 'styles'],
        templates = ['dustf'];

    gulp.watch('./public/assets/styles/scss/*.scss', styles);
    gulp.watch('./public/assets/templates/*.dust', templates);

});

gulp.task('test', ['jshint', 'mocha', 'phantom', 'casper']);

gulp.task('build', ['clean', 'imagemin', 'dustb', 'dustf', 'sass', 'styles']);
