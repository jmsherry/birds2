define(["BirdsApp", "apps/messaging/list/list_view"], function(BirdsApp, View) {
    BirdsApp.module("MessagingApp.List", function(List, BirdsApp, Backbone, Marionette, $, _) {
        List.Controller = {
            list: function(messages) {
                var messageCollection, messagesView;

                messageCollection = Backbone.Collection.extend();
                messages = new messageCollection(messages);
                messagesView = new View.Messages({
                    collection: messages
                });

                BirdsApp.messaging.show(messagesView);
            }
        };
    });

    return BirdsApp.MessagingApp.List.Controller;
});