var connectionURL = require('../../config/database').url,
    mongoose = require('mongoose'),
    db = mongoose.connection,
    options = {
        replset: {},
        server: {}
    };

options.server.socketOptions = options.replset.socketOptions = {
    keepAlive: 1
};

//console.log(db);
db.on('error', function(error) {
    db.on('error', console.error.bind(console, 'connection error:'));
});

mongoose.connect(connectionURL);

module.exports.db = db;