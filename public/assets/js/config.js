// Set the require.js configuration for your application.
require.config({

    paths: {
        modernizr: './../bower_components/modernizr/modernizr',
        jquery: './../bower_components/jquery/dist/jquery',
        'jquery-ui': './../bower_components/jquery-ui/jquery-ui',
        '_': './../bower_components/lodash/dist/lodash.underscore',
        backbone: './../bower_components/backbone/backbone',
        'backbone.babysitter': './../bower_components/backbone.babysitter/lib/backbone.babysitter',
        'backbone.wreqr': './../bower_components/backbone.wreqr/lib/backbone.wreqr',
        localstorage: './../bower_components/backbone.localstorage/backbone.localStorage',
        'backbone-syphon': './../bower_components/backbone.syphon/lib/amd/backbone.syphon',
        'backbone-picky': './../bower_components/backbone.picky/lib/amd/backbone.picky',
        bootstrap: './../bower_components/bootstrap/dist/js/bootstrap',
        marionette: './../bower_components/backbone.marionette/lib/core/backbone.marionette',
        'marionette-dust': './../bower_components/backbone.marionette.dust/src/amd/backbone.marionette.dust',
        'dustjs-linkedin': './../bower_components/dustjs-linkedin/dist/dust-full.min',
        'cdjs-helpers': './helpers/custom_dust_helpers/bundle',
        'dust-helpers': './../bower_components/dustjs-helpers/dist/dust-helpers',
        'dust-date-helper': './helpers/custom_dust_helpers/helper-dateFormat',
        spin: './../bower_components/spin.js/spin',
        'spin.jquery': './../bower_components/spin.js/jquery.spin',
        url: './../bower_components/js-url/url',
        json3: './../bower_components/json3/lib/json3',
        'jquery.lazyload': './../bower_components/jquery.lazyload/jquery.lazyload',
        'bootstrap-tour': './../bower_components/bootstrap-tour/build/js/bootstrap-tour.min',
        templates: './../templates/compiled/templates',
        async: './../bower_components/requirejs-plugins/src/async',
        io: './../bower_components/socket.io-client/socket.io',
        moment: './../bower_components/moment/moment',
        timepicker: './../bower_components/jqueryui-timepicker-addon/dist/jquery-ui-timepicker-addon',
        offline: './../bower_components/offline/offline.min',
        BirdsApp: 'app' //this calls in the main file
    },

    shim: {
        '_': {
            exports: '_'
        },
        jquery: {
            exports: '$'
        },
        'jquery-ui': {
            deps: ['jquery'],
            exports: '$'
        },
        backbone: {
            deps: ['jquery', '_', 'json3'],
            exports: 'Backbone'
        },
        bootstrap: {
            deps: ['jquery'],
            exports: 'bootstrap'
        },
        'bootstrap-tour': {
            deps: ['jquery'],
            exports: 'bootstrap-tour'
        },
        'backbone-babysitter': {
            deps: ['backbone', '_'],
            exports: 'backbone-babysitter'
        },
        'dustjs-linkedin': {
            deps: [],
            exports: 'dust'
        },
        'cdjs-helpers': {
            deps: ['dustjs-linkedin'],
            exports: 'cdjs-helpers'
        },
        'dust-helpers': {
            deps: ['dustjs-linkedin'],
            exports: 'dust-helpers'
        },
        'dust-date-helper': {
            deps: ['dustjs-linkedin', 'moment', 'cdjs-helpers'],
            exports: 'dust-date-helper'
        },
        templates: {
            deps: ['cdjs-helpers', '_', 'marionette-dust', 'dust-date-helper'],
            exports: 'templates'
        },
        timepicker: {
            deps: ['jquery', 'jquery-ui'],
            exports: 'timepicker'
        },
        // 'marionette-dust': {
        //     deps: ['marionette', 'dustjs-linkedin', 'backbone'],
        //     exports: 'marionette-dust'
        // },
        marionette: {
            deps: ['backbone', '_'],
            exports: 'Marionette'
        },
        localstorage: {
            deps: ['backbone'],
            exports: 'localstorage'
        },
        // 'backbone-picky': {
        //     deps: ['backbone'],
        //     exports: 'backbone-picky'
        // },
        // 'backbone-syphon': {
        //     deps: ['backbone'],
        //     exports: 'backbone-syphon'
        // },
        // 'backbone.wreqr': {
        //     deps: ['backbone'],
        //     exports: 'backbone-wreqr'
        // },
        // 'backbone.babysitter': {
        //     deps: ['backbone', '_'],
        //     exports: 'backbone-babysitter'
        // },
        url: {
            deps: [],
            exports: 'url'
        },
        moment: {
            deps: [],
            exports: 'moment'
        },
        io: {
            deps: [],
            exports: 'io'
        },
        "spin.jquery": {
            deps: ["spin", "jquery"]
        },
        'jquery.lazyload': {
            deps: ['jquery'],
            exports: 'lazyload'
        },
        offline:{
            exports:'Offline'
        },
        BirdsApp: {
            deps: ['localstorage', 'bootstrap', 'cdjs-helpers', 'jquery-ui', 'dust-helpers', 
            'dust-date-helper', 'marionette-dust', 'templates', 'timepicker', 'bootstrap-tour', 
            'spin.jquery', 'url', 'async', 'io', 'offline'],
            exports: 'BirdsApp'
        }
    },

    map: {
        '*': {
            'underscore': '_',
            'lodash': '_',
            'dust': 'dustjs-linkedin'
        }
    },
    waitSeconds: 15

});

require(["BirdsApp", "./apps/auth/auth_app", "./apps/header/header_app", "./apps/typeLinks/typeLinks_app", "./apps/messaging/messaging_app"], function(BirdsApp) {
    //console.log(BirdsApp);
    BirdsApp.start();
});