var updateRankings = require('cron').CronJob,
    mongoose = require('mongoose'),
    Site = mongoose.model('Location'),
    User = mongoose.model('User'),
    Bird = mongoose.model('Bird'),
    _ = require('lodash');

new updateRankings('* * * * *', function() {
        console.log('update rankings cron task running...');

        var users = User.find({}).populate('spots').exec(function(err, users) {
            //console.log(users);
            var totalUsers = users.length,
                opts = {
                    path: 'spots.bird',
                    model: 'Bird'
                };


            User.populate(users, opts, function(err, users) {
                if (err) {
                    console.log(err);
                    return;
                }

                console.log(users);

                users.forEach(function(user) {
                    var points = 0,
                        spots = user.spots,
                        spotStatus,
                        length = spots.length;

                    for (var i = 0; i < length; i += 1) {
                        spotStatus = spots[i].bird.status;
                        switch (spotStatus) {
                            case 'secure':
                                points += 1;
                                //console.log('secure bird');
                                break;
                            case 'rare':
                                points += 5;
                                //console.log('rare bird');
                                break;
                        }
                    }

                    user.ranking.total = totalUsers;
                    user.points = points;
                    user.save(function(err, user) {
                        if (err) {
                            console.log(err);
                            return;
                        }

                        //console.log('Saved user: ', user.username);
                    });

                });


            });

        });

        var orderedUsers = User.find({}).sort('ranking.position').exec(function(err, users) {
            //console.log(arguments);
            var i, length = users.length,
                thisUser;
            for (i = 0; i < length; i += 1) {
                thisUser = users[i];
                thisUser.ranking.position = (i + 1);
                thisUser.save();
            }

        });

    },
    null, true, "United Kingdom/London");