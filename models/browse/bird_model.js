// load the things we need
var mongoose = require('mongoose'),
    mongooseTimes = require("mongoose-times");

// define the schema for our user model
var birdSchema = mongoose.Schema({
    description: String,
    fullDescription: String,
    name: String,
    social: String,
    status: String,
    subtype: String,
    type: String,
    lifespan: {
        min: Number,
        max: Number,
        unit: String
    },
    weight: {
        min: Number,
        max: Number,
        display: {
            min: Number,
            max: Number,
            unit: {
                full: String,
                abbr: String
            }
        }
    },
    wingspan: {
        min: Number,
        max: Number,
        display: {
            min: Number,
            max: Number,
            unit: {
                full: String,
                abbr: String
            }
        }
    },
    length: {
        min: Number,
        max: Number,
        display: {
            min: Number,
            max: Number,
            unit: {
                full: String,
                abbr: String
            }
        }
    }
});

birdSchema.plugin(mongooseTimes);


// create the model for users and expose it to our app
module.exports = mongoose.model('Bird', birdSchema);