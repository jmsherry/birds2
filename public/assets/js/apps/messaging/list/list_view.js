define(["BirdsApp"],
    function(BirdsApp) {
        BirdsApp.module("MessagingApp.List.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.Message = Marionette.ItemView.extend({
                tagName: 'li',
                template: 'message.dust',
                onRender: function(view){
                    var status = view.model.attributes.status;
                    switch(status){
                        case 'error':
                        view.$el.addClass('bg-danger');
                        break;
                        default:
                        view.$el.addClass('bg-' + status);
                        break;
                    };
                },

                serializeData: function(){
                    console.log('OPTIONS', this.options);
                    return this.options.model.toJSON();
                }
            });

            View.Messages = Marionette.CompositeView.extend({
                //template: 'messages.dust',
                tagName: "ul",
                childView: View.Message
            });

 
        });

        return BirdsApp.MessagingApp.List.View;
    });