"use strict";
// This file is required by app.js. It sets up event listeners
// for the two main URL endpoints of the application - /create and /chat/:id
// and listens for socket.io messages.

// Use the gravatar module, to turn email addresses into avatar images:

var gravatar = require('gravatar'),
    moment = require('moment'),
    Backbone = require('backbone'),
    Mongoose = require('mongoose'),
    Comment = Mongoose.model('Comment');

// Export a function, so that we can pass 
// the app and io instances from the app.js file:

module.exports = function(server, app) {

var io = require('socket.io')(server),

        chatroomLimit = app.get('chatroomLimit'),
        chat, users, comments, existingComments;

//local dev settings
    //io.set('origins', 'localhost:*')
      //  .set('transports', ['websocket', 'xhr-polling', 'jsonp-polling', 'htmlfile', 'flashsocket', 'polling']);

//Heroku settings
        io.set('transports', ['websocket', 'xhr-polling', 'jsonp-polling', 'htmlfile', 'flashsocket', 'polling']);


    users = new Backbone.Collection();
    comments = new Backbone.Collection();


    // Initialize a new socket.io application, named 'chat'
    chat = io.of('/socket').on('connection', function(socket) {
        var thisUser;

        //Client logs in
        socket.on('join', function(user) {
            thisUser = user;
            console.log('received join request', user);
            if(!users.get(user._id)){
                console.log('adding user');
                users.add(user);
            }
            socket.broadcast.emit('newUser', users.models, user);
            chat.emit('conversation', {
                comments: comments.models
            });
            return thisUser;
        });

        // Handle the sending of messages
        socket.on('message', function(message) {
            //console.log('received message', message);
            comments.add(message);
            console.log('comments', comments);
            chat.emit('conversation', {
                comments: comments.models
            });
        });

        socket.on('disconnect', function() {
            console.log('disconnected user', thisUser);
            chat.emit('disconnect', thisUser);
            users.remove(thisUser);
        });

    });
    return server;
};