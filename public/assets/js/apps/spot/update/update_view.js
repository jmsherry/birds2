define([
        "BirdsApp"
    ],
    function(BirdsApp) {
        // console.log('in browse view', arguments);

        BirdsApp.module("Spots.Update.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.updateSpot = Backbone.Marionette.ItemView.extend({

                template:"sighting_form.dust",

                events: {
                    "submit form": "updateSpot",
                },

                serializeData: function(){
                    var newBirds=[], newSites=[], oldBirds = this.options.birds, oldSites = this.options.sites;

                    oldSites.forEach(function(site){
                        var s = site.attributes;
                        newSites.push(s);
                    });

                    oldBirds.forEach(function(bird){
                        var b = bird.attributes;
                        newBirds.push(b);
                    });

                    return {
                        birds: newBirds,
                        locations: newSites,
                        user: localStorage.getItem('currentUser'),
                        title: this.options.title
                    };

                },

                updateSpot: function(){
                    console.log('updating spot');
                    var newSpot = Backbone.Syphon.serialize(this);
                    newSpot.id = this.options.spotId;
                    console.log(this.options.spotId, newSpot);
                    BirdsApp.trigger('spot:update', newSpot);
                    return false;
                }

            });
        });

        return BirdsApp.Spots.Update.View;
    });