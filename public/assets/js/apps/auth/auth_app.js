define(["BirdsApp", "apps/auth/setup/setup_controller", "apps/auth/signup/signup_controller", "apps/auth/authenticate/auth_controller", "entities/user"],
    function(BirdsApp, SetupController, SignUpController, AuthController, ProfileController) {
        BirdsApp.module("AuthApp", function(AuthApp, BirdsApp, Backbone, Marionette, $, _) {
            AuthApp.startWithParent = false;

            AuthApp.onStart = function() {
                console.log("starting AuthApp");
            };

            AuthApp.onStop = function() {
                console.log("stopping AuthApp");
            };
        });
        BirdsApp.module("Routers.AuthApp", function(AuthRouter, BirdsApp, Backbone, Marionette, $, _) {
            AuthRouter.Router = Marionette.AppRouter.extend({
                appRoutes: {
                    "signup": "signupSetup"
                }
            });

            var API = {
                setup: function(user) {
                    console.log('setup called');
                    SetupController.setup(user);
                },
                signupSetup: function() {
                    console.log('setting up signup');
                    SignUpController.setup();
                },
                signupShow: function() {
                    console.log('to controller');
                    SignUpController.showSignUpPage();
                },
                signup: function(user) {
                    SignUpController.signup(user);
                },
                login: function(user) {
                    AuthController.login(user);
                },
                logout: function() {
                    AuthController.logout();
                },
                authenticateFrontEnd: function(user, state) {
                    AuthController.authenticateFrontEnd(user, state);
                },
                authenticateFailed: function(message) {
                    AuthController.authenticateFailed(message);
                },
                confirmAuthenticated: function() {
                    AuthController.confirmAuthenticated();
                }
            };

            BirdsApp.on("signup:show", function() {
                API.signupShow();
            }).on("signup:submit", function(user) {
                console.log('signup:submit event');
                API.signup(user);
            }).on("signup:success", function(user) {
                console.log('signup:success event');
                BirdsApp.trigger('browse:list');
            }).on("setup:authenticate", function(user) {
                console.log('setup authenticated!');
                API.setup(user);
            }).on("login:success", function(user) {
                console.log('authenticated!');
                API.authenticateFrontEnd(user, true);
            }).on("login:failure", function(message) {
                console.log('Failed Auth: ', message);
                API.authenticationFailed(message);
            }).on("login", function(user) {
                console.log('login event');
                API.login(user);
            }).on("logout", function() {
                console.log('logout event');
                API.logout();
            }).on("confirmAuthenticated", function() {
                console.log('confirmAuthenticated event');
                API.confirmAuthenticated();
            }).addInitializer(function() {
                new AuthRouter.Router({
                    controller: API
                });
            });

            AuthRouter.on("start", function() {
                API.confirmAuthenticated();
                //API.setup();
            });
        });

        return BirdsApp.AuthApp;
    });