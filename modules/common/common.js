var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    _ = require('lodash');

module.exports = function(app, db) {
    app.get('/menuitems', function(req, res) {
        res.send(app.get('appviews'));
    });

    app.get('/data/types', function(req, res) {
        res.send(app.get('typeMatrix'));
    });

    return app;
};