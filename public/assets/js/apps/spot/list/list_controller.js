define(["BirdsApp", "apps/spot/list/list_view"], function(BirdsApp, View) {
    BirdsApp.module("Spots.List", function(List, BirdsApp,
        Backbone, Marionette, $, _) {
        List.Controller = {
            listSpots: function() {
                console.log('in listcontroller', arguments);
                require(["common/views", "entities/spot", "entities/user"], function(CommonViews) {

                    var spinner = new CommonViews.Loading();
                    BirdsApp.content.show(spinner);

                    var fetchingspots = BirdsApp.request("spot:entities");
                    $.when(fetchingspots).done(function(spots) {

                            var spotsListView;

                            spotsListView = new View.spots({
                                collection: spots,
                                itemviewOptions: {
                                    user: BirdsApp.currentUser
                                }
                            });

                            console.log('spotsListView: ', spotsListView);
                            BirdsApp.content.show(spotsListView);

                    });
                });
            }
        };
    });

    return BirdsApp.Spots.List.Controller;
});