define(["BirdsApp", "apps/header/list/list_view"], function(BirdsApp, View) {
    BirdsApp.module("HeaderApp.List", function(List, BirdsApp, Backbone, Marionette, $, _) {
        List.Controller = {
            showMenu: function(state) {
                console.log('SHOW MENU FUNCTION');
                require(["entities/header"], function() {

                    var links = BirdsApp.request("header:entities");
                    var headers = new View.Headers({
                        collection: links,
                        itemViewOptions: {
                            authenticated: state
                        }
                    });

                    headers.on("itemview:navigate", function(childView, model) {
                        console.log(arguments);
                        var trigger = model.get("navigationTrigger");
                        BirdsApp.trigger(trigger);
                    });

                    BirdsApp.nav.show(headers);
                });
            },

            showNonAuthMenu: function() {
                console.log('in logout headers');
                var links = BirdsApp.request("header:entities");
                var headers = new View.Headers({
                    collection: links,
                    itemViewOptions: {
                        authenticated: false
                    }
                });

                console.log('nonAuth: ', headers);

                headers.on("itemview:navigate", function(childView, model) {
                    var trigger = model.get("navigationTrigger");
                    BirdsApp.trigger(trigger);
                });

                BirdsApp.nav.show(headers);
            },

            setActiveHeader: function(headerUrl) {
                require(["entities/header", "common/views"], function(header, CommonViews) {
                    var links = BirdsApp.request("header:entities");
                    var headerToSelect = links.find(function(header) {
                        return header.get("name") === headerUrl;
                    });
                    headerToSelect.select();
                    links.trigger("reset");
                });
            }
        };
    });

    return BirdsApp.HeaderApp.List.Controller;
});