// load the things we need
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var ObjectId = mongoose.Schema.Types.ObjectId;

// define the schema for our user model
var userSchema = mongoose.Schema({

    avatar: String,
    comments: [{
        type: ObjectId,
        ref: 'Comment'
    }],
    currentLocation: {
        type: ObjectId,
        ref: 'Location'
    },
    points: Number,
    ranking: {
        position: Number,
        total: Number
    },
    spots: [{
        type: ObjectId,
        ref: 'Sighting'
    }],
    username: String,

    local: {
        email: String,
        password: String,
    },
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    twitter: {
        id: String,
        token: String,
        displayName: String,
        username: String
    },
    google: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    confirmed: Boolean

});

// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);