define([
        "BirdsApp",
        "apps/sites/list/list_view"
    ],
    function(BirdsApp, listView) {
        // console.log('in browse view', arguments);

        BirdsApp.module("Spots.Add.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.addSpot = Backbone.Marionette.ItemView.extend({

                template:"sighting_form.dust",

                events: {
                    "submit form": "addSpot",
                },

                serializeData: function(){
                    var newBirds=[], newSites=[], options = this.options, oldBirds = options.birds, oldSites = options.sites;

                    oldSites.forEach(function(site){
                        var s = site.attributes;
                        newSites.push(s);
                    });

                    oldBirds.forEach(function(bird){
                        var b = bird.attributes;
                        newBirds.push(b);
                    });

                    return {
                        birds: newBirds,
                        locations: newSites,
                        user: localStorage.getItem('currentUser'),
                        title: options.title,
                        selected: options.selected
                    };

                },

                onRender:function(){
                    //select the selected bird
                },

                addSpot: function(){
                    console.log('adding spot');
                    var newSpot = Backbone.Syphon.serialize(this);
                    BirdsApp.trigger('spots:add', newSpot);
                    return false;
                }

            });
        });

        return BirdsApp.Spots.Add.View;
    });