define(["BirdsApp", "apps/spot/remove/remove_view"], function(BirdsApp, View) {
    BirdsApp.module("Spots.Remove", function(Remove, BirdsApp,
        Backbone, Marionette, $, _) {
        Remove.Controller = {
            removeSpots: function(id, $DOMNode) {
                //remove the spot

                if(window.confirm("Do you really want to delete this spot?!")){
                    require(["entities/spot"], function() {

                        var fetchingspots = BirdsApp.request("spot:entities");

                        $.when(fetchingspots).done(function(spots) {
                            var spot = spots.get(id);
                            // spot.destroy({
                            //     success: function(){
                            //         $DOMNode.fadeOut().remove();
                            //     }
                            // });
                            spot.destroy();
                        });

                    });
                }
            }
        };
    });

    return BirdsApp.Spots.Remove.Controller;
});