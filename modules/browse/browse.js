var mongoose = require('mongoose'),
    Bird = mongoose.model('Bird');

module.exports = function(app, db) {

    app.set('renderResults', function(err, birds, req, res, type, subtype, name, pointer, breadcrumbs) {
        //console.log('in renderResults: ', birds.length);

        var result, displayLimit, pagination, paginationNext, paginationPrev, searchTerm, tmpl,
            resultLength, soloView, startPoint, endPoint, renderVars, pageTotals, pagURLs;

        if (!birds || !Array.isArray(birds)) {
            console.log("No birds found, no error.");
            birds = [];
        }

        pageTotals = '';
        soloView = false;
        pointer = pointer ? pointer * 1 : 0;
        resultLength = birds.length;
        displayLimit = app.get('displayLimit');
        pagination = paginationNext = paginationPrev = false;
        startPoint = pointer * displayLimit;
        endPoint = startPoint + displayLimit;

        //sort birds alpabetically
        birds = app.get('sortByKey')(birds, 'name');

        function getPaginationTargetURI(pointer, type, subtype, name) {
            var urls, urlStem = '/browse',
                newType, newSubtype, newName;

            if (type) {
                newType = encodeURIComponent(type);
                urlStem += '/' + newType;
            }

            if (subtype) {
                newSubtype = encodeURIComponent(subtype);
                urlStem += '/' + newSubtype;
            }

            if (name) {
                newName = encodeURIComponent(name);
                urlStem += '/' + newName;
            } else {
                urlStem += '?p=';
            }

            urls = {
                next: urlStem + (pointer + 1),
                prev: urlStem + (pointer - 1)
            };

            return urls;
        }

        pagURLs = getPaginationTargetURI(pointer, type, subtype, name);

        //console.log('resultLength: ', resultLength, ' displayLimit: ', displayLimit, ' pointer: ', pointer);

        //console.log(birds);

        if (resultLength > displayLimit) {
            // console.log('Need for pagination');
            pagination = true;
            if (startPoint > 0) {
                //   console.log('turning on prev');
                paginationPrev = true;
            }

            if (endPoint < resultLength) {
                //  console.log('turning on next');
                paginationNext = true;
            }
        }
        //console.log('pagination: ', pagination, ' paginationPrev: ', paginationPrev, ' paginationNext: ', paginationNext);
        if (name) {
            soloView = true;
        }

        //console.log('startPoint', startPoint, 'endPoint', endPoint);
        birds = birds.slice(startPoint, endPoint);

        startPoint += 1;

        if (endPoint >= resultLength) {
            if (startPoint === resultLength) {
                pageTotals = startPoint + ' of ' + resultLength;
            } else {
                pageTotals = startPoint + ' to ' + resultLength + ' of ' + resultLength;
            }
        } else {
            pageTotals = startPoint + ' to ' + endPoint + ' of ' + resultLength;
        }

        tmpl = 'birds_list_container';
        renderVars = {
            'authenticated': req.isAuthenticated(),
            'loginForm': true,
            'birds': birds,
            'appviews': app.get('appviews'),
            'subtype': subtype,
            'type': type,
            'name': name,
            'breadcrumbs': breadcrumbs,
            'pagination': pagination,
            'paginationNext': paginationNext,
            'paginationPrev': paginationPrev,
            'nextURL': pagURLs.next,
            'prevURL': pagURLs.prev,
            'soloView': soloView,
            'queryString': '?p=' + pointer,
            'pageTotals': pageTotals,
            'thisView': 'browse'
        };

        renderVars = app.get('getRenderVars')(renderVars);
        //console.log(renderVars);
        res.render(tmpl, renderVars);
    });


    //ROUTES
    //redirect on first entry
    app.get('/', function(req, res) {
        console.log('In root');
        res.redirect('/browse?p=0');
    });

    app.get('/browse/:type?/:subtype?/:name?', function(req, res) {
        //console.log('in browse');
        // console.log('in browse', arguments);

        if (app.get('queryStringCheck')(res, req)) {
            var name, birds, searchTerm, pointer, type, subtype, reqParams;
            reqParams = req.params;


            type = app.get('decode')(reqParams.type);
            subtype = app.get('decode')(reqParams.subtype);
            name = app.get('decode')(reqParams.name);
            searchTerm = app.get('searchTerm')(name, subtype, type);
            pointer = req.query.p;

            birds = Bird.find(searchTerm, function(err, birds) {
                app.get('renderResults')(err, birds, req, res, type, subtype, name, pointer, true);
            });

        }
    });


    //REST ROUTES
    app.get('/data/birds/:type?/:subtype?/:name?', function(req, res) {
        //console.log('data:birds');
        var options, type, subtype, name, reqParams;

        function callback(err, birds) {
            res.send(birds);
        }

        reqParams = req.params;

        type = app.get('decode')(reqParams.type);
        subtype = app.get('decode')(reqParams.subtype);
        name = app.get('decode')(reqParams.name);

        //console.log(type, subtype, name);

        if (!type && !subtype && !name) {
            options = {};
        } else {
            options = {
                type: type,
                subtype: subtype,
                name: name
            };
        }
        Bird.find(options, callback);
    });

    //OUT OF SCOPE
    // app.post('/search', function (req, res) {

    // });

    // app.get('/search', function(req, res) {
    //     app.get('handleAuthentication')(req, res);
    //     console.log('in search');
    //     var renderVars = {};
    //     renderVars = app.get('getRenderVars')(renderVars);
    //     res.render('search', renderVars);
    // });

    return app;

};