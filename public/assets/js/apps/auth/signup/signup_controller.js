define(["BirdsApp", "apps/auth/signup/signup_view"], function(BirdsApp, View) {
    BirdsApp.module("AuthApp.Signup", function(Signup, BirdsApp, Backbone, Marionette, $, _) {
        Signup.Controller = {
            setup: function() {
                var signup = new View.SignupSetup();
                //console.log('HEREEEE', signup);
            },
            showSignUpPage: function() {
                console.log('in showSignUpPage');
                BirdsApp.trigger('menu:deselect');
                require(["common/views"], function(CommonViews) {

                    var loadingView = new CommonViews.Loading();
                    BirdsApp.content.show(loadingView);

                    var signup = new View.Signup();
                    console.log(signup);
                    BirdsApp.content.show(signup);
                });
            },
            signup: function(user) {
                console.log('signing up', arguments);
                $.ajax('/async/signup', {
                    type: "POST",
                    //contentType: 'application/json',
                    data: user
                }).success(function(resp) {
                    console.log(arguments);
                    if (resp) {
                        BirdsApp.trigger('signup:success', resp);
                    } else {
                        BirdsApp.trigger('signup:failure', resp);
                    }

                }).error(function(resp) {
                    console.log(arguments);
                    BirdsApp.trigger('signup:failure:error', resp);
                });
                return false;
            }
        };
    });

    return BirdsApp.AuthApp.Signup.Controller;
});