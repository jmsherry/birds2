define(["BirdsApp", "apps/browse/setup/setup_view"], function(BirdsApp, View) {
    BirdsApp.module("Browse.Setup", function(Setup, BirdsApp,
        Backbone, Marionette, $, _) {
        Setup.Controller = {
            setupBirds: function() {
                var birdsSetupView = new View.birdsSetup(),
                    pagination = new View.pagination(),
                    breadcrumbs = new View.breadcrumbs();
            }
        };
    });

    return BirdsApp.Browse.Setup.Controller;
});