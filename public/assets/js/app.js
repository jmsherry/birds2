define([
        // Libraries.
        'cdjs-helpers',
        'marionette',
        'dust-date-helper',
        'apps/config/marionette/regions/dialog'
    ],

    function(helpers, Marionette) {
        'use strict';

        //prep work:
        helpers.export_helpers_to(dust);

        // Provide a global location to place configuration settings and module creation.
        var BirdsApp = new Marionette.Application();
        BirdsApp.root = '/';

        // Queueing system for offline
        BirdsApp.utilities = {
            persistence: {
                fullSync: function () {
                    BirdsApp.offlineQueue.toBeDeleted.forEach(function(model){
                        model.ajaxSync("delete", this, {
                            'url':this.urlRoot + '/' + this.id,
                            success: function() {
                                console.log('SUCCESS DELETE STUB');
                                this.destroy();
                            },
                            error: errorReporter
                        });
                    });

                    BirdsApp.offlineQueue.toBeModified.forEach(function(model){
                        model.ajaxSync("update", this, {
                            'url':this.urlRoot + '/' + this.id,
                            success: function() {
                                console.log('SUCCESS UPDATE STUB');
                                //this.destroy();
                            },
                            error: errorReporter
                        });
                    });

                    BirdsApp.offlineQueue.toBeAdded.forEach(function(model){
                        model.ajaxSync("create", this, {
                            'url':this.urlRoot,
                            success: function() {
                                console.log('SUCCESS ADD STUB');
                                //this.destroy();
                            },
                            error: errorReporter
                        });
                    });
                }
            }
        };

        BirdsApp.offlineQueue = {
            toBeDeleted: Backbone.Collection.extend(),
            toBeModified: Backbone.Collection.extend(),
            toBeAdded:Backbone.Collection.extend()
        };

        // window.Offline = {
        //     state: "up"
        // }
        $(function() {
        // All navigation that is relative should be passed through the navigate
        // method, to be processed by the router. If the link has a `data-bypass`
        // attribute, bypass the delegation completely.
            $(document.body).on("click.specNav", "a:not([data-bypass])", function(evt) {
                evt.preventDefault();
                var href = $(this).attr("href");
                console.log('special nav', href);
                BirdsApp.navigate(href);
                $('#flash ul').children().remove();
            });

            $('body').append('<div id="dialog-region"></div>');

            //offline sensor
            var $online = $('.online'),
                $offline = $('.offline');

            Offline.on('confirmed-down', function () {
                $online.fadeOut(function () {
                    $offline.fadeIn();
                });
            });

            Offline.on('confirmed-up', function () {
                $offline.fadeOut(function () {
                    $online.fadeIn();
                });
            });

            Offline.on('on', function(){
                BirdsApp.utilities.persistence.fullSync();
            });

            Offline.options = {
              // Should we check the connection status immediatly on page load.
              checkOnLoad: false,

              // Should we monitor AJAX requests to help decide if we have a connection.
              interceptRequests: true,

              // Should we automatically retest periodically when the connection is down (set to false to disable).
              reconnect: {
                // How many seconds should we wait before rechecking.
                initialDelay: 3,

                // How long should we wait between retries.
                delay: (1.5)
              },

              // Should we store and attempt to remake requests which fail while the connection is down.
              requests: true,

              // Should we show a snake game while the connection is down to keep the user entertained?
              // It's not included in the normal build, you should bring in js/snake.js in addition to
              // offline.min.js.
              game: false
            };

        });



        var FadeInRegion = Backbone.Marionette.Region.extend({

            open: function(view) {
                this.$el.hide();
                this.$el.html(view.el);
                this.$el.fadeIn();
            }

        });

        BirdsApp.addRegions({
            wrapper: '#wrapper',
            content: FadeInRegion.extend({
                el: "#main-content"
            }),
            nav: FadeInRegion.extend({
                el: "#main-nav-container"
            }),
            messaging: '#flash',
            header: '#main-header',
            usermenu: '#user-menu',
            auth: FadeInRegion.extend({
                el: "#auth-container"
            }),
            breadcrumbs: FadeInRegion.extend({
                el: "#breadcrumbs"
            }),
            footerLinks: '#type-links',
            dialogRegion: Marionette.Region.Dialog.extend({
                el: "#dialog-region"
            })
        });

        BirdsApp.navigate = function(route, opts) {
            var options = opts || {};
            Backbone.history.navigate(route, options);
        };

        BirdsApp.getCurrentRoute = function() {
            return Backbone.history.fragment;
        };

        BirdsApp.startSubApp = function(appName, args) {
            var currentApp = appName ? BirdsApp.module(appName) : null;
            if (BirdsApp.currentApp === currentApp) {
                return;
            }

            if (BirdsApp.currentApp) {
                BirdsApp.currentApp.stop();
            }

            BirdsApp.currentApp = currentApp;
            if (currentApp) {
                currentApp.start(args);
            }
        };

        BirdsApp.config = {
            'imgSizes': {
                'large': '&s=200',
                'small': '&s=50'
            }
        };

        BirdsApp.currentUser = {};

        BirdsApp.updated = {
            birds: false,
            sites: false,
            spots: false,
            users: false,
            comments: false
        };

        BirdsApp.on("start", function() {

            if (Backbone.history) {
                require([
                    "./apps/browse/browse_app",
                    "./apps/chat/chat_app",
                    "./apps/sites/sites_app",
                    "./apps/spot/spot_app",
                    "./apps/profile/profile_app"
                ], function() {
                    Backbone.history.start({
                        pushState: true,
                        root: BirdsApp.root
                    });

                    //BirdsApp.request('user:entities');

                    if (BirdsApp.getCurrentRoute() === "browse?p=0") {
                        BirdsApp.trigger("browse:setup");
                    }
                });


            }

        });


//console.log('BirdsApp', BirdsApp);
        return BirdsApp;

    });