    define(["BirdsApp"], function(BirdsApp) {
    BirdsApp.module("SpotApp", function(SpotApp, BirdsApp, Backbone, Marionette, $, _) {
        SpotApp.startWithParent = false;

        SpotApp.onStart = function() {
            console.log("starting SpotApp");
        };

        SpotApp.onStop = function() {
            console.log("stopping SpotApp");
        };
    });
    BirdsApp.module("Routers.SpotApp", function(SpotsRouter, BirdsApp, Backbone, Marionette, $, _) {
        SpotsRouter.Router = Marionette.AppRouter.extend({
            appRoutes: {
                // "spot/add/:id": "add",
                // "spot/update/:id": "update",
                "spots": "setup" //,
            }
        });

        var executeAction = function(action, arg) {
            BirdsApp.startSubApp("SpotApp");
            action(arg);
            BirdsApp.execute("set:active:header", "spot");
        };

        var API = {
            setup: function(options) {
                BirdsApp.trigger('messages:show', [{"status":"error", "message": "Error Test"}, 
                    {"status":"info", "message": "Info Test"}, {"status":"success", "message": "Success Test"}]);
                console.log('in spots setup: ', arguments);
                require(["apps/spot/setup/setup_controller"], function(SetupController) {
                    executeAction(SetupController.setup, options);
                });
            },
            list: function(options) {
                console.log('in spots list: ', arguments);
                require(["apps/spot/list/list_controller"], function(ListController) {
                    executeAction(ListController.listSpots, options);
                });
            },
            updateForm: function(options) {
                console.log('in spots updateForm: ', arguments);
                require(["apps/spot/update/update_controller"], function(UpdateController) {
                    UpdateController.updateForm(options);
                });
            },
            update: function(options) {
                console.log('in spots update: ', arguments);
                require(["apps/spot/update/update_controller"], function(UpdateController) {
                    UpdateController.update(options);
                });
            },
            remove: function(id, $DOMNode) {
                console.log('in spots remove: ', arguments);
                require(["apps/spot/remove/remove_controller"], function(RemoveController) {
                    RemoveController.removeSpots(id, $DOMNode);
                });
            },
            addForm: function(options) {
                console.log('in spots addForm: ', arguments);
                require(["apps/spot/add/add_controller"], function(AddController) {
                    AddController.addForm(options);
                });
            },
            add: function(vals){
                console.log('in spots add: ', arguments);
                require(["apps/spot/add/add_controller"], function(AddController) {
                    AddController.add(vals);
                });
            }
        };

        BirdsApp.on("spots:setup", function() {
            console.log('spots:setup');
            API.setup();
        }).on("spots:list", function() {
            console.log('spots:list');
            //BirdsApp.navigate("/spots");
            API.list();
        }).on("spots:addForm", function(id) {
            console.log('spots:addForm');
            API.addForm(id);
        }).on("spots:add", function(vals) {
            console.log('spots:add', vals);
            API.add(vals);
        }).on("spot:remove", function(id, $DOMNode) {
            console.log('spot:remove');
            API.remove(id, $DOMNode);
        }).on("spot:updateForm", function(id) {
            console.log('spot:updateForm');
            API.updateForm(id);
        }).on("spot:update", function(newVals) {
            console.log('spot:update', newVals);
            API.update(newVals);
        });

        BirdsApp.addInitializer(function() {
            new SpotsRouter.Router({
                controller: API
            });
        });
    });

    return BirdsApp.SpotsRouter;
});