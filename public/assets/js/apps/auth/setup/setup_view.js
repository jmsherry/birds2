define(["BirdsApp", "backbone-syphon"],
    function(BirdsApp) {
        BirdsApp.module("AuthApp.Setup.View", function(View, BirdsApp, Backbone, Marionette, $, _) {
            View.AuthPanel = Marionette.ItemView.extend({

                el: "#auth-panel",

                events: {
                    "click .signup a": "signup",
                    //"click #login-form input[type=submit]": "login",
                    "submit form": "login",
                    "click a.slideTrigger": "slideToggle"
                },

                initialize: function(){
                    if($(window).width() < 1200){
                        $('#auth-container')
                        .append('<div class="row"><a href="#auth-container" class="slideTrigger btn btn-info btn-lg btn-block" data-bypass>Sign up or Log in</a></div>')
                        .on('click', this.slideToggle)
                        .children('#auth-panel').slideUp();
                    }
                },

                slideToggle: function(e){
                    $('#auth-container').children('#auth-panel').slideToggle({
                        duration:400,
                        complete: function(){
                            if($('#auth-panel').is(':visible')){
                                $(e.target).text('close');
                            } else {
                                $(e.target).text('Sign up or Log in');
                            }
                        }
                    });
                    return false;
                },

                signup: function() {
                    console.log('setup signing up');
                    BirdsApp.trigger("signup:show");
                    return false;
                },

                login: function(e) {
                    console.log('setup login');
                    var user = Backbone.Syphon.serialize(this);
                    console.log(user);
                    BirdsApp.trigger("login", user);
                    return false;
                },

                render: function() {
                    this.bindUIElements();
                }
            });

            View.UserMenu = Marionette.ItemView.extend({

                el: '#user-menu',

                events: {
                    "click .profile a": "profile",
                    "click .logout a": "logout"
                },

                logout: function() {
                    console.log('logging out');
                    BirdsApp.trigger("logout");
                    return false;
                },

                profile: function() {
                    console.log('profile');
                    BirdsApp.trigger("profile", {
                        user: 'currentUser'
                    });
                    return false;
                },

                initialize: function(){
                    $(this.el).find('.logout a').attr('data-bypass', "");
                },

                render: function() {
                    console.log('rendering UserMenu');
                    this.bindUIElements();
                }
            });

        });
        return BirdsApp.AuthApp.Setup.View;
    });