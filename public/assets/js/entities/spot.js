define([
    'BirdsApp',
    'apps/config/storage/localstorage'
], function(BirdsApp) {
    //console.log('entitySpotsArgs: ', arguments);
    BirdsApp.module("Entities", function(Entities, BirdsApp, Backbone, Marionette, $, _) {

        function errorReporter(err) {
            //console.log(err);
            alert('Error: ' + err.status + ' ' + err.statusText + '\n' + err.responseText + ' \nPlease refresh the page or contact the maintainer');
        }

        function constructSpot(model) {
            var bird, site;

            
            if(localStorage){

                //get bird
                localBird = localstorage.getItem(model.urlRoot + '-' + model.get('bird'));
                if(localBird.length > 0){
                    bird = localBird;
                }

                //get location
                localSite = localstorage.getItem(model.urlRoot + '-' + model.get('site'));
                if(localSite){
                    site = localSite;
                }

            } else {
                console.log('No localStorage; trying server call');
                if(Offline.state === 'up'){

                } else {
                    throw new Error('No Conn or localstorage');
                }

            }
            //set in model
            model.set('bird', bird);
            model.set('site', site);
            model.set('user', BirdsApp.currentUser);

            return model;
        }

        Entities.Spot = Backbone.Model.extend({

            urlRoot: "data/spots",
            idAttribute: "_id",
            ajaxSync: Backbone.ajaxSync,
            defaults: {
                user: BirdsApp.currentUser.get('_id')
            },

            onDestroy: function(){
                //BirdsApp.updated.spots = false;
                console.log('model onDestroy stub');
                var currentModel = this;
                if (Offline.state === 'up'){
                    this.ajaxSync("delete", this, {
                        'url':this.urlRoot + '/' + this.id,
                        success: function() {
                            console.log('SUCCESS DELETE STUB');
                        },
                        error: errorReporter
                    });
                } else {
                    BirdsApp.offlineQueue.toBeDeleted.add(this);
                }
                //TODO remove DOM node...
            },
            onChange: function(){
                //BirdsApp.updated.spots = false;
                console.log('model onChange stub');
                if (Offline.state === 'up'){
                this.ajaxSync("update", this, {
                    'url':this.urlRoot + '/' + this.id,
                    success: function() {
                        console.log('SUCCESS UPDATE STUB');
                    },
                    error: errorReporter
                });
                } else {
                    BirdsApp.offlineQueue.toBeModified.add(this);
                }
            },
            onAdd: function(){
                //BirdsApp.updated.spots = false;
                console.log('model onAdd', arguments);
                if (Offline.state === 'up'){
                    this.ajaxSync("create", this, {
                        'url':this.urlRoot,
                        success: function() {
                            console.log('SUCCESS ADD STUB');
                        },
                        error: errorReporter
                    });
                } else {
                    BirdsApp.offlineQueue.toBeAdded.add(this);
                }


                this.save();
                //TODO add DOM node...
            },
            initialize: function() {
                //var owner;
                //console.log('the model: ', this);
                this.on("destroy", function(){
                    console.log('model destroy evt received: ', arguments);
                    this.onDestroy();
                }).on("add", function(){
                    console.log('model add evt received: ', arguments);
                    this.onAdd();
                }).on("change", function(){
                    console.log('model change evt received: ', arguments);
                    this.onChange();
                });

                
                //owner = BirdsApp.currentUser.get('_id');
                //console.log('currentUser', owner);
                //this.set('user', owner);

            }

        });

        Entities.configureStorage(Entities.Spot);

        Entities.SpotCollection = Backbone.Collection.extend({
            model: Entities.Spot,
            ajaxSync: Backbone.ajaxSync,
            comparator: "name",
            url: "data/spots",
            initialize: function(){
                this.on('remove', function(model, collection, options){
                    console.log('col remove evt: stub', arguments);
                    //this.sync("delete", model);
                    //this.sync('update', this);
                   // BirdsApp.trigger("spots:list");
                }).on('change', function(model, collection, options){
                    console.log('col change evt: stub', arguments);
                    //this.sync("update", model);
                    //this.sync('update', this);
                    //BirdsApp.trigger("spots:list");
                }).on('add', function(model, collection, options){
                    console.log('col add evt: stub', arguments);
                    //this.sync("create", model);
                    //this.sync('update', this);
                    //BirdsApp.trigger("spots:list");
                });
            },
            getPoints: function(){
                var score = 0;
                this.forEach(function(model){
                    var status = model.get('status');
                    if (status === 'rare'){
                        score += 5;
                    } else if(status === 'secure'){
                        score += 1;
                    }
                });
                return score;
            }
        });

        Entities.configureStorage(Entities.SpotCollection);


        var API = {
            getSpotEntities: function() {

                var spots = new Entities.SpotCollection(),
                    defer = $.Deferred(),
                    storedSpots = localStorage.getItem('data/spots');

                if (storedSpots === null || !BirdsApp.updated.spots && Offline.state === 'up') {
                    console.log('ajaxSync spots');
                    //Fetch via Ajax
                    spots.ajaxSync("read", spots, {
                        success: function(data) {
                            console.log('data', data);
                            // _.forEach(data, function(spot) {
                            //     var thisSpot = new Entities.Spot(spot);
                            //     spots.add(thisSpot, {silent:true});
                            // });

                            spots.reset(data);

                            spots.forEach(function(spot) {
                                spot.save();
                            });

                            BirdsApp.updated.spots = true;

                            defer.resolve(spots);
                        },
                        error: errorReporter
                    });

                } else {
                    console.log('local sync spots');

                    spots.fetch({
                        success: function(collection, response, options) {
                            //console.log('fetched spots: ', collection.length, arguments);
                            defer.resolve(collection);
                        },
                        error: errorReporter
                    });


                }
                return defer.promise();
            }
        };

        BirdsApp.reqres.setHandler("spot:entities", function() {
            return API.getSpotEntities();
        });

    });

    return;
});