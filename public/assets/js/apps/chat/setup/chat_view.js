define(["BirdsApp", "io", "moment"],
    function(BirdsApp, io, moment) {

        BirdsApp.module("Chat.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            function updateTimes() {
                //console.log('updating time');
                $(".commentTime").each(function() {
                    //console.log(this, $(this).data('time'));
                    var time = moment($(this).data('time'));
                    $(this).text(time.fromNow());
                });

            }

            View.comment = Backbone.Marionette.ItemView.extend({
                template: 'comment.dust',
                tagName: "li",
                updateTimes: updateTimes,
                onShow: function() {
                    this.updateTimes();
                }
            });

            View.empty = Backbone.Marionette.ItemView.extend({
                template: 'no_comments.dust',
                tagName: "li"
            });

            View.joined = Backbone.Marionette.ItemView.extend({
                template: 'joined.dust',
                tagName: "li",
                className:'joined info',
                initialize: function(){
                    console.log('initializing joined view');
                    this.render();
                    this.show();
                }
            });

            View.exited = Backbone.Marionette.ItemView.extend({
                template: 'exited.dust',
                tagName: "li",
                className:'exited info',
                initialize: function(){
                    console.log('initializing exited view');
                    this.render();
                    this.show();
                    //$('#comments-list').append('<li>Exited</li>')
                }
            });

            View.comments = Backbone.Marionette.CompositeView.extend({
                template: 'chatroom.dust',
                className: "row",
                childViewContainer: '#comments-list',
                childView: View.comment,
                emptyView: View.empty,
                childViewOptions: this.options.itemViewOptions,
                events: {
                    "keypress #comment": "inputText",
                    "submit #chat-form": "submitForm"
                },

                $chatform: $("#chat-form"),

                initialize: function(collection) {
                    this.listenTo(this.collection, "reset", function() {
                        console.log('in the reset function');
                        this.render();
                    });

                    this.appendHtml = function(itemView) {
                        console.log('in appendHtml', arguments);
                        this.$el.find(this.childViewContainer).append(itemView.el);
                    };
                },

                updateTimes: updateTimes,

                serializeData: function() {
                    //console.log('HERHE HERHER HERHERE', this.options);
                    //this.options.lastUpdated = moment(this.options.lastUpdated).fromNow();
                    console.log('HERHE HERHER HERHERE', this.options);
                    this.socket = this.options.itemViewOptions.socket;
                    this.currentUser = this.options.itemViewOptions.currentUser;

                    this.options.authenticated = true; //remember to remove
                    return this.options;
                },


                inputText: function(e) {
                    console.log('pressing', e.which);
                    // Submit the form on enter
                    if (e.which === 13) {
                        console.log('you  hit enter');
                        e.preventDefault();
                        this.$chatform.trigger('submit');
                    }
                },

                submitForm: function() {
                    console.log('in submitForm');
                    var newComment, user;
                    //console.log(typeof BirdsApp.currentUser, BirdsApp.currentUser);

                    if(BirdsApp.currentUser){
                        user = BirdsApp.currentUser.toJSON();
                    } else {
                        user = {};
                    }
                    // Create a new chat message and display it directly
                    newComment = {
                        comment: $("#comment").val(),
                        user: user,
                        source: 'BirdsApp',
                        time: moment()
                    };
                    console.log('newComment', newComment);
                    //this.collection.add(newComment);

                    // Send the message to the other person in the chat
                    this.socket.emit('message', newComment);

                    // Empty the $textarea
                    $("#comment").val("");
                    return false;
                },

                onShow: function() {
                
                    this.updateTimes();
                    // Update the relative time stamps on the chat messages every minute
                    setInterval(this.updateTimes, 60000);

                }

            });

        });

        return BirdsApp.Chat.View;
    });