define(['dustjs-linkedin'], function() {
    var CreateCommonDustjsHelpers = function() {
        "use strict";

        var __bind = function(fn, me) {
            return function() {
                return fn.apply(me, arguments);
            };
        };

        function CommonDustjsHelpers() {
            this.first_helper = __bind(this.first_helper, this);
            this.downcase_helper = __bind(this.downcase_helper, this);
            this.titlecase_helper = __bind(this.titlecase_helper, this);
            this.upcase_helper = __bind(this.upcase_helper, this);
            this.despace_helper = __bind(this.despace_helper, this);
            this.respace_helper = __bind(this.respace_helper, this);
            this.get_helpers = __bind(this.get_helpers, this);
            this.export_helpers_to = __bind(this.export_helpers_to, this);
        }

        CommonDustjsHelpers.dust = null;


        CommonDustjsHelpers.prototype.export_helpers_to = function(dust) {
            dust.helpers = this.get_helpers(dust.helpers);
            CommonDustjsHelpers.dust = dust;
            return CommonDustjsHelpers;
        };

        CommonDustjsHelpers.prototype.get_helpers = function(helpers) {
            if (helpers === null) {
                helpers = {};
            }
            helpers['if'] = this.if_helper;
            helpers.unless = this.unless_helper;
            helpers.upcase = helpers.UPCASE = this.upcase_helper;
            helpers.downcase = this.downcase_helper;
            helpers.titlecase = helpers.Titlecase = this.titlecase_helper;
            helpers.filter = this.filter_helper;
            helpers.count = this.count_helper;
            helpers.repeat = this.repeat_helper;
            helpers.first = this.first_helper;
            helpers.last = this.last_helper;
            helpers.even = this.even_helper;
            helpers.odd = this.odd_helper;
            helpers.respace = this.respace_helper;
            helpers.despace = this.despace_helper;
            if (helpers.sep === null) {
                helpers.sep = this.classic_sep;
            }
            if (helpers.idx === null) {
                helpers.idx = this.classic_idx;
            }
            return helpers;
        };


        CommonDustjsHelpers.prototype.upcase_helper = function(chunk, context, bodies, params) {
            return chunk.capture(bodies.block, context, function(data, chunk) {
                chunk.write(data.toUpperCase());
                return chunk.end();
            });
        };

        CommonDustjsHelpers.prototype.titlecase_helper = function(chunk, context, bodies, params) {
            //console.log('titlecase helper called');
            return chunk.capture(bodies.block, context, function(data, chunk) {
                chunk.write(data.replace(/([^\W_]+[^\s-]*) */g, (function(txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1);
                })));
                return chunk.end();
            });
        };

        CommonDustjsHelpers.prototype.downcase_helper = function(chunk, context, bodies, params) {
            return chunk.capture(bodies.block, context, function(data, chunk) {
                chunk.write(data.toLowerCase());
                return chunk.end();
            });
        };


        CommonDustjsHelpers.prototype.despace_helper = function(chunk, context, bodies, params) {
            return chunk.capture(bodies.block, context, function(data, chunk) {
                chunk.write(decodeURIComponent(data));
                return chunk.end();
            });
        };

        CommonDustjsHelpers.prototype.respace_helper = function(chunk, context, bodies, params) {
            return chunk.capture(bodies.block, context, function(data, chunk) {
                chunk.write(encodeURIComponent(data));
                return chunk.end();
            });
        };

        return CommonDustjsHelpers;
    }();


    var cdjsHelpers = new CreateCommonDustjsHelpers(); //executes class to create obj
    //console.log(cdjsHelpers);

    return cdjsHelpers;
});