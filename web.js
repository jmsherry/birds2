"use strict";

// Monitoring software: https://rpm.newrelic.com/accounts/670431/applications/setup?destination=web
//require('newrelic');

//Initial setup
var path, http, port, server, cdjshelp, util, cookies, oauth, twitter, crypto, _, options, express, auth, lodash, dust, dustjs,
    dustjsHelpers, commonDustjsHelpers, app, db, fs, mongoose, Comment, Bird, Sighting, Site, User,
    Backbone, io, dateTimeHelper, moment;

//node modules, express and dust declarations
path = require('path');
util = require('util');
fs = require('fs');
http = require('http');
crypto = require('crypto');
cookies = require('cookies');
oauth = require('oauth');
_ = lodash = require('lodash');
dust = require('dustjs-linkedin');
dustjsHelpers = require('dustjs-helpers');
dustjs = require('adaro');
Backbone = require('backbone');
twitter = require('./resources/APIs/twitter/twitter');
moment = require('moment');

//Non-standard bespoke modules

dateTimeHelper = require('./templates/helpers/custom_dust_helpers/helper-dateFormat')(dust, moment);
cdjshelp = require('./templates/helpers/common-dustjs-helpers2').cdjsHelpers;
cdjshelp.export_helpers_to(dust);


//console.log(dust.helpers);

//APP Defn...
app = require('./config/appSetup')(dustjs);

//Database
db = require('./resources/database/db');

//App Variables & Config items
app = require('./modules/config/config')(app, db);

//Helper Functions
app = require('./modules/helpers/helpers')(app, _);

//console.log(dust);

//Models
Comment = require('./models/chat/comment_model');
Bird = require('./models/browse/bird_model');
Sighting = require('./models/spot/sighting_model');
Site = require('./models/sites/site_model').location;
User = require('./modules/auth/models/user');

//Controllers (Modules)

/* At the top, with other redirect methods before other routes */
app.all('*', function(req, res, next) {
    if (req.headers['x-forwarded-proto'] != 'https')
        res.redirect('https://' + req.headers.host + req.url);
    else
        next(); /* Continue to other routes if we're not redirecting */
});

/*** COMMON MODULE ***/
app = require('./modules/common/common')(app, db);

/*** BROWSE MODULE ***/
app = require('./modules/browse/browse')(app, db);

/*** CHAT MODULE ***/
app = require('./modules/chat/chat')(app, db); //synchronous chat - bulletin board

/*** SPOT MODULE ***/
app = require('./modules/spot/spot')(app, db);

/*** SITES MODULE ***/
app = require('./modules/sites/sites')(app, db);

/*** AUTH MODULE ***/
app = require('./modules/auth/routes/routes')(app, db);


//******** SERVER CONFIG **********//
port = process.env['PORT'] = process.env.PORT || 4000; // Used by https on localhost

server = http.createServer(app).listen(port, function() {
    console.log("Express server listening with http on port %d in %s mode", this.address().port, app.settings.env);
});

require('./modules/chat/routes')(server, app); //web sockets server