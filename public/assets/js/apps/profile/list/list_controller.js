define(["BirdsApp", "apps/profile/list/list_view"], function(BirdsApp, View) {
    BirdsApp.module("AuthApp.Profile", function(Profile, BirdsApp, Backbone, Marionette, $, _) {
        Profile.Controller = {
            profile: function(user) {

                var profile = new View.UserProfile({
                    user: user
                });
                console.log('HERE HERE', profile);

                BirdsApp.content.show(profile);

            }
        };
    });

    return BirdsApp.AuthApp.Profile.Controller;
});