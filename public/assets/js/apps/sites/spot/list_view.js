define(["BirdsApp" //,
        // "dustc!/assets/templates/no_results_browse.dust",
        // "dustc!/assets/templates/birds_list_container.dust",
        // "dustc!/assets/templates/bird_description.dust"
    ],
    function(BirdsApp) {
        // console.log('in browse view', arguments);

        BirdsApp.module("Birds.List.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.bird = Backbone.Marionette.ItemView.extend({
                tagName: "li",
                template: 'bird_description.dust',

                events: {
                    "click.options": "options",
                    "click.type .typeLink ": "type",
                    "click.subtype .subtypeLink ": "subtype"
                },
                options: function() {
                    alert('options');
                    return false;
                },
                type: function() {
                    alert('type');
                    return false;
                },
                subtype: function() {
                    alert('subtype');
                    return false;
                }
            });

            View.birds = Backbone.Marionette.CompositeView.extend({
                $el: $("#results_list"),
                template: 'birds_list_container',
                tagName: 'ul',
                id: "#results_list",
                className: "results_list",
                itemView: View.bird,

                initialize: function() {
                    console.log('view args: ', arguments);
                    this.listenTo(this.collection, "reset", function() {
                        this.appendHtml = function(collectionView, itemView, index) {
                            console.log(arguments);
                            collectionView.$el.append(itemView.el);
                        };
                    });
                },

                onCompositeCollectionRendered: function() {
                    console.log(arguments);
                    this.appendHtml = function(collectionView, itemView, index) {
                        collectionView.$el.prepend(itemView.el);
                    };
                }
            });
        });

        return BirdsApp.Birds.List.View;
    });