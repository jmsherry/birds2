define(["BirdsApp"],
    function(BirdsApp) {
        BirdsApp.module("HeaderApp.Setup.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.Brand = Marionette.ItemView.extend({
                el: '#main-header',
                events: {
                    "click h1 a": "brand"
                },
                brand: function() {
                    console.log('brand');
                    this.trigger('brand:clicked');
                    //BirdsApp.navigate('/browse?p=0');
                    return false;
                },
                render: function() {
                    this.bindUIElements();
                }
            });

            View.mainNav = Marionette.ItemView.extend({
                el: $('#main-nav'),
                initialize: function() {
                    console.log('INITIALISE MAIN NAV');
                },

                events: {
                    "click.setup a": "navigate"
                },

                navigate: function(e) {
                    //e.stopPropagation();
                    console.log('navigating');
                    console.log(e.target.attributes.href.value);
                    var thisLink = e.target;
                    var href = thisLink.attributes.href.value;
                    var action = href.substring(1).split('?');
                    action = action[0].split('/');
                    action = action[0];
                    console.log('action: ', action);
                    switch (action) {

                        case 'browse':
                            action = 'browse:list';
                            break;
                        case 'chatroom':
                            action = 'chat:setup';
                            break;
                        case 'sites':
                            action = 'sites:list';
                            break;
                        case 'spots':
                            action = 'spots:list';
                            break;
                        case 'default':
                            action = 'browse:list';
                            break;

                    }
                    console.log('action: ', action);
                    $('#main-nav').find('.active').removeClass('active');
                    $(thisLink).addClass('active');
                    //BirdsApp.navigate(href);
                    BirdsApp.trigger("navigating");
                    BirdsApp.trigger(action, {
                        url: href
                    });
                    return false;
                },

                render: function() {
                    this.bindUIElements();
                }
            });


        });

        return BirdsApp.HeaderApp.Setup.View;
    });