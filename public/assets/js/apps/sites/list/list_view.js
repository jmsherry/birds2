define(["BirdsApp", "async!https://maps.google.com/maps/api/js?sensor=false&&libraries=places"],
    function(BirdsApp) {

        BirdsApp.module("Sites.List.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.sites = Backbone.Marionette.ItemView.extend({

                template: 'sites',
                tagName: "div",
                id: 'map-canvas',
                className: 'map-canvas',

                events: {

                },

                onShow: function() {
                    console.log('RENDERING');

                    //document.getElementById('main-content').innerHtml = ''; //destroy google map each time



                    //console.log(this.collection);
                    var mapOptions, map, input, searchBox, marker, infoWindow = null,
                        contentString = null,
                        site, sites, sitesLength, $width, $map;

                    sites = this.collection.models;


                    mapOptions = {
                        zoom: 6,
                        center: new google.maps.LatLng(53, 0)
                    };

                    mapOptions = _.extend(mapOptions, this.options);
                    map = new google.maps.Map(this.el, mapOptions);
                    google.maps.event.trigger(map, "resize");




                    function setMarker(map, site) {
                        var thisSite = site,
                            atts = thisSite.attributes,
                            marker, contentString, infoWindow;

                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(atts.position.Lat, atts.position.Long),
                            map: map,
                            title: atts.placename
                        });

                        contentString = '<div class=\"infoWindow\">' +
                            '<h1>' + atts.site + ': ' + atts.placename + '</h1>' +
                            '<p>' + atts.information + '</p></div>';

                        infoWindow = new google.maps.InfoWindow({});
                        infoWindow.setContent(contentString);

                        google.maps.event.addListener(marker, 'click', function() {
                            console.log('this: ', this);
                            infoWindow.open(map, marker);
                        });
                    }

                    function setMarkers(map, sites) {
                        var i, sitesLength = sites.length;
                        for (i = 0; i < sitesLength; i++) {
                            setMarker(map, sites[i]);
                        }
                    }

                    setMarkers(map, sites);

                }
            });
        });

        return BirdsApp.Sites.List.View;
    });