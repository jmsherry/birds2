var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Comment = mongoose.model('Comment'),
    Twitter = require("node-twitter"),
    util = require('util'),
    creds = require('./../../config/auth').twitterAuth,
    Moment = require('moment');

creds = {
    consumer_key: creds.consumerKey,
    consumer_secret: creds.consumerSecret,
    access_token_key: creds.accessTokenKey,
    access_token_secret: creds.accessTokenSecret
};

//console.log('creds: ', creds);

module.exports = function(app, db) {

    function chatroom(req, res) {
        console.log('in chatroom function', 'req.method: ', req.method);
        var comments = null,
            renderVars, newComment, commentsArray = [],
            user, messages = '';



        if (req.method && req.method === 'POST') {
            if (!req.user) {
                req.flash('authInfo', ['Please login to post']);
                console.log('not logged in');
            } else {

                console.log('in POST part', req.user);
                user = User.findOne({
                    '_id': req.user._id
                }, function(err, user) {
                    console.log('the user: ', user);
                    console.log('the comment', req.body);
                    newComment = new Comment({
                        'comment': req.body.comment,
                        'user': user._id,
                        'source': 'BirdsApp'
                    });

                    newComment.save(function(err, newComment) {
                        if (err) return console.error(err);
                        console.dir(newComment);
                        req.messages = messages;
                        res.redirect('/chatroom');
                    });
                });


            }

        }




    }

    app.get('/chatroom', function(req, res) {
        // console.log('in chatroom GET');
        // chatroom(req, res);
        var comments = Comment.find({}).populate('user').exec(function(error, comments) {

            // var twitterSearchClient = new Twitter.SearchClient(creds.consumerKey, creds.consumerSecret, creds.accessTokenKey, creds.accessTokenSecret);
            // twitterSearchClient.search({
            //         'q': '#BirdsApp'
            //     },
            //     function(error, data) {
            var tweets = [];
            //         if (error) {
            //             console.log(error);
            //             req.flash('error', 'Problem connecting with Twitter. Will try again the next time you refresh...');
            //         } else {
            //             console.log(data);
            //             console.log('tweets: ', util.inspect(data.statuses)); //console.log('logged in: ', req.isAuthenticated());
            //             //comments.push(data.statuses);
            //             tweets = data.statuses;
            //         }

            var renderVars = {
                'messages': req.flash(),
                'authenticated': req.isAuthenticated(),
                'loginForm': true,
                'appviews': app.get('appviews'),
                'comments': comments,
                'thisView': 'chatroom',
                'tweets': tweets
            };
            renderVars = app.get('getRenderVars')(renderVars);
            //console.log(renderVars);
            res.render('chatroom', renderVars);
            //});
        });
    });

    app.post('/chatroom', function(req, res) {
        console.log('in chatroom POST');
        chatroom(req, res);
    });

    //REST ROUTES
    app.get('/data/comments', function(req, res) {
        //console.log('data:comments');
        var callback = function(err, comments) {
            //console.log(comments);
            res.send(comments);
        };
        Comment.find({}).populate('user').exec(callback);
    });

    // app.post('/data/comments', function(req, res) {
    //     chatroom(req, res);
    // });

    app.get('/twitter', function(req, res, next) {
        //console.log(twitter);
        // twitter.stream('filter', {
        //     delimited: 200,
        //     track: 'cat'
        // }, function(stream) {
        //     stream.on('data', function(data) {
        //         console.log('bang');
        //         // try {
        //         //     console.log(data);
        //         //     res.render('twitter', {
        //         //         'tweets': data
        //         //     });
        //         // } catch (e) {
        //         //     console.log(e);
        //         // }
        //     });
        // });

        twitter.search('#BirdsApp', function(data) {
            console.log(util.inspect(data.statuses));
            res.render('twitter', {
                'tweets': data.statuses
            });
        });
    });

    return app;
};