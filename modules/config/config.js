var mongoose = require('mongoose'),
    Site = mongoose.model('Location'),
    User = mongoose.model('User'),
    Bird = mongoose.model('Bird'),
    _ = require('lodash');

module.exports = function(app, db) {

    //App variables
    app.set('appviews', [{
        'name': 'browse',
        'isPrivate': false,
        'destinationURL': '/browse?p=0'
    }, {
        'name': 'sites',
        'isPrivate': false,
        'destinationURL': '/sites'
    }, {
        'name': 'spot',
        'isPrivate': true,
        'destinationURL': '/spots'
    }, {
        'name': 'chatroom',
        'isPrivate': false,
        'destinationURL': '/chatroom'
    }]);

    app.set('typeMatrix', [{
        "type": "birds of prey",
        "subtypes": [{
            "subtype": "eagles"
        }, {
            "subtype": "kites"
        }]
    }, {
        "type": "garden birds",
        "subtypes": [{
            "subtype": "sparrows"
        }, {
            "subtype": "tits"
        }, {
            "subtype": "crows"
        }, {
            "subtype": "warblers"
        }, {
            "subtype": "finches"
        }]
    }, {
        "type": "waterfowl",
        "subtypes": [{
            "subtype": "swans"
        }, {
            "subtype": "waders"
        }, {
            "subtype": "ducks"
        }]
    }]);

    app.set('baseview', {
        layout: 'index',
        appviews: app.get('appviews'),
        year: new Date().getUTCFullYear(),
        types: app.get('typeMatrix'),
        environment: app.settings.env,
        messages: {},
        'large-img': app.get('large-img'),
        'small-img': app.get('small-img'),
        'authenticated': true
    });

    //display limite for browse pagination
    app.set('displayLimit', 9);

    //Limit for chatroom users
    app.set('chatroomLimit', 10);

    app.set('large-img', '&s=200');

    app.set('small-img', '&s=50');

    //Cron Jobs
    //require('./../../config/cron_jobs/cron')(app, db);

    app.set('updateRankings', function() {
        console.log('update rankings cron task running...');

        var users = User.find({}).populate('spots').exec(function(err, users) {
            //console.log(users);
            var totalUsers = users.length,
                opts = {
                    path: 'spots.bird',
                    model: 'Bird'
                };


            User.populate(users, opts, function(err, users) {
                if (err) {
                    console.log(err);
                    return;
                }

                console.log(users);

                users.forEach(function(user) {
                    var points = 0,
                        spots = user.spots,
                        spotStatus,
                        length = spots.length;

                    for (var i = 0; i < length; i += 1) {
                        spotStatus = spots[i].bird.status;
                        switch (spotStatus) {
                            case 'secure':
                                points += 1;
                                //console.log('secure bird');
                                break;
                            case 'rare':
                                points += 5;
                                //console.log('rare bird');
                                break;
                        }
                    }

                    user.ranking.total = totalUsers;
                    user.points = points;
                    user.save(function(err, user) {
                        if (err) {
                            console.log(err);
                            return;
                        }

                        console.log('Saved user: ', user.username);
                    });

                });


            });

        });

        var orderedUsers = User.find({}).sort('ranking.position').exec(function(err, users) {
            //console.log(arguments);
            var i, length = users.length,
                thisUser;
            for (i = 0; i < length; i += 1) {
                thisUser = users[i];
                thisUser.ranking.position = (i + 1);
                thisUser.save();
            }

        });

    });

//app.get('updateRankings')();

    return app;
};