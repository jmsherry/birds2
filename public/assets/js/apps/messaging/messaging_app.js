define(["BirdsApp", "apps/messaging/list/list_controller"], function(BirdsApp, ListController) {
    BirdsApp.module("MessagingApp", function(HeaderApp, BirdsApp, Backbone, Marionette, $, _) {

        var API = {
            list: function(messages) {
                ListController.list(messages);
            }
        };

        BirdsApp.on('messages:show', function(messages) {
            console.log('messages:show');
            API.list(messages);
        }).on('navigating', function() {
            console.log('cleaning messages on navigation...');
            $('#flash ul').children().remove();
        });

    });

    return BirdsApp.MessagingApp;
});