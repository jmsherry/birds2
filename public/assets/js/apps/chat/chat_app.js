define(["BirdsApp"], function(BirdsApp) {
    BirdsApp.module("Chat", function(Chat, BirdsApp, Backbone, Marionette, $, _) {
        Chat.startWithParent = false;

        Chat.onStart = function() {
            console.log("starting Chat");
        };

        Chat.onStop = function() {
            console.log("stopping Chat");
            BirdsApp.trigger('chat:leave', BirdsApp.currentUser);
        };
    });
    BirdsApp.module("Routers.Chat", function(ChatRouter, BirdsApp, Backbone, Marionette, $, _) {
        ChatRouter.Router = Marionette.AppRouter.extend({
            appRoutes: {
                "chatroom": "setup"
            }
        });

        var executeAction = function(action, arg) {
            BirdsApp.startSubApp("Chat");
            action(arg);
            BirdsApp.execute("set:active:header", "chatroom");
        };

        var API = {
            setup: function(options) {
                require(["apps/chat/setup/chat_controller"], function(ChatController) {
                    console.log('setup ChatController', ChatController);
                    executeAction(ChatController.setup, options);
                });
            }
        };

        BirdsApp.on("chat:setup", function(options) {
            console.log('chat:setup');
            API.setup(options);
        });

        BirdsApp.addInitializer(function() {
            new ChatRouter.Router({
                controller: API
            });
        });
    });

    return BirdsApp.Chat;
});