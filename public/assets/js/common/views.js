define(["BirdsApp" /*, "spin"*/ ], function(BirdsApp) {
    BirdsApp.module("Common.Views", function(Views, BirdsApp, Backbone, Marionette, $, _) {
        Views.Loading = Marionette.ItemView.extend({
            template: 'loading.dust',

            initialize: function(opts) {
                var options = opts || {};
                this.title = options.title || "Loading Data";
                this.message = options.message || "Please wait, data is loading.";
            },

            serializeData: function() {
                return {
                    title: this.title,
                    message: this.message
                };
            },

            onShow: function() {
                var opts = {
                    lines: 13, // The number of lines to draw
                    length: 20, // The length of each line
                    width: 10, // The line thickness
                    radius: 30, // The radius of the inner circle
                    corners: 1, // Corner roundness (0..1)
                    rotate: 0, // The rotation offset
                    direction: 1, // 1: clockwise, -1: counterclockwise
                    color: "#000", // #rgb or #rrggbb
                    speed: 1, // Rounds per second
                    trail: 60, // Afterglow percentage
                    shadow: false, // Whether to render a shadow
                    hwaccel: false, // Whether to use hardware acceleration
                    className: "spinner", // The CSS class to assign to the spinner
                    zIndex: 2e9, // The z-index (defaults to 2000000000)
                    top: "50%", // Top position relative to parent in px
                    left: "50%" // Left position relative to parent in px
                };
                $('[data-spin]').spin(opts);
            }
        });

        Views.message = Marionette.ItemView.extend({
            tagName: 'li',
            template: 'message.dust',
            onRender: function() {
                if (this.options.error) {
                    this.$el.addClass('bg-danger');
                } else if (this.options.info) {
                    this.$el.addClass('bg-info');
                } else if (this.options.success) {
                    this.$el.addClass('bg-success');
                }
            }
        });

        Views.messages = Marionette.CompositeView.extend({
            template: 'messages.dust',
            childView: Views.message,
            childViewContainer: '#messages-panel'//,
            // initialize: function() {

            // },
            // serializeData: function() {

            // }

        });
    });

    return BirdsApp.Common.Views;
});