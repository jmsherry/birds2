var assert = require("assert");
var Backbone = require('backbone');
var bird = {};

beforeEach(function() {
    var Bird = Backbone.Model.extend({

        urlRoot: "data/birds",
        idAttribute: "_id",
        ajaxSync: Backbone.ajaxSync,
        meta: function(prop, value) {
            if (value === undefined) {
                return this._meta[prop];
            } else {
                this._meta[prop] = value;
            }
        },
        initialize: function() {
            this._meta = {};
        },
        validate: function(attrs, options) {

        }

    });

    bird = new Bird();
    // console.log(bird);
    // return bird;
});

describe('Bird', function() {

    it('should have a urlRoot of \'data/birds\'', function() {
        assert.equal(bird.urlRoot, 'data/birds');
    });

});