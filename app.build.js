({
    //appDir: 'public',
    mainConfigFile: "public/assets/js/config.js",
    baseUrl: "public/assets/js",
    locale: "en-gb",
    // modules: [
    //     {
    //         name: "config"
    //         include: ['require', templates]
    //     }
    // ],
    uglify2: {
        toplevel: false,
        output: {
            beautify: true
        },
        compress: {
            sequences: false,
            // global_defs: {
            //     DEBUG: false
            // }
        },
        warnings: true,
        mangle: false
    },
    name: "config",
    fileExclusionRegExp: /^\./,
    removeCombined: true,
    findNestedDependencies: true,
    out: "./dist/assets/scripts/config.js",
    optimize: "uglify2",
    preserveLicenseComments: false,
    generateSourceMaps: true
})