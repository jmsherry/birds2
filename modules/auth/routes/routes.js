var _ = require('lodash'),
    mail = require('./../../mail/mail').sendMail,
    passport = require('passport'),
    Mongoose = require('mongoose'),
    User = Mongoose.model('User');



module.exports = function(app) {

    var confirmURL = "https://" + app.get('host') + ':' + app.get('port') + "/signup/confirm";
    //var confirmURL = "https://birdsapp.heroku.com/signup/confirm";

    // route middleware to ensure user is logged in
    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();

        res.redirect('/');
    }



    // normal routes ===============================================================

    // Route to check authentication status
    app.get('/authconfirm', function(req, res) {
        if (req.isAuthenticated()) {
            res.send(req.user);
        } else {
            res.send(false);
        }
    });

    //route to send sanitised user data
    app.get('/data/users', function(req, res) {
        //console.log('data/users');
        var userIDs = req.body.users;
        //console.log(userIDs);

        var callback = function(err, users) {
            if (err) {
                res.send(err);
            } else if (users) {
                _.forEach(users, function(user) {
                    user.local = null;
                });
                res.send(users);
            } else {
                res.send({
                    'message': 'Serious Error!!'
                });
            }
        };

        User.find({}, callback);

    });




    // PROFILE SECTION =========================
    app.get('/profile', isLoggedIn, function(req, res) {
        var options = app.get('baseview');
        var safeUser = req.user;
        safeUser.local.password = null;
        options = _.extend(options, {
            user: safeUser,
            authenticated: true,
            thisView: null
        });
        //console.log(options);
        res.render('profile.dust', options);
    });

    // LOGOUT ==============================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/browse?p=0');
    });

    app.get('/async/logout', function(req, res) {
        req.logout();
        res.send(true);
    });

    // =============================================================================
    // AUTHENTICATE (FIRST LOGIN) ==================================================
    // =============================================================================

    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile', // redirect to the secure profile section
        failureRedirect: '/browse?p=0', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));


    app.post('/async/login', function(req, res, next) {
        // console.log('async call args: ' /*, arguments*/ );
        // if (req.user) {
        //     res.send(req.user);
        // } else {
        passport.authenticate('local-login', function(err, user, info, status) {
            //console.log('in async login', arguments);

            if (err) {
                return res.send(err);
            }

            if (user) {
                user.local = null;
                return res.send(user);

            } else {
                return res.send(info);
            }

        })(req, res, next);

        //}
    });

    // SIGNUP =================================
    // show the signup form
    app.get('/signup', function(req, res) {
        //console.log('in correct route');
        var renderVars = {
            message: req.flash('loginMessage')
        };
        renderVars = app.get('getRenderVars')(renderVars);
        //console.log(renderVars);
        res.render('signup.dust', renderVars);
    });

    // process the signup form synchronously
    // app.post('/signup', passport.authenticate('local-signup', {
    //     successRedirect: '/profile', // redirect to the secure profile section
    //     failureRedirect: '/browse?p=0', // redirect back to the signup page if there is an error
    //     failureFlash: true // allow flash messages
    // }));

    app.post('/signup', function(req, res, next) {
        passport.authenticate('local-signup', function(err, user, info, status) {
            //console.log('in async signup', arguments);

            if (err) {
                res.render('error', {
                    "error": err
                });
            }

            if (user) {
                mail({
                    "to": user.email,
                    "redirectURL": confirmURL + '?user=' + user._id
                }, "signup");
            }

            res.redirect('browse?p=0');


        })(req, res, next);
    });

    app.get('/signup/confirm/:id', function(req, res, next) {

        var User = require('./models/user');
        User.findOne({
            'id': req.id
        }, function(err, user) {
            if (err) {
                res.redirect('error', err);
            }

            if (user) {
                user.confirmed = true;
                user.save();
                req.flash("success", "Email address confirmed");
                res.redirect('browse?p=0');
            }
        });

    });

    // process the signup form asynchronously
    app.post('/async/signup', function(req, res, next) {
        passport.authenticate('local-signup', function(err, user, info, status) {
            //console.log('in async signup', arguments);

            if (err) {
                res.send(err);
            }

            if (user) {
                res.send(user);

            } else {
                res.send(info);
            }



        })(req, res, next);
    });

    // facebook -------------------------------

    // send to facebook to do the authentication
    app.get('/auth/facebook', passport.authenticate('facebook', {
        scope: 'email'
    }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));

    // twitter --------------------------------

    // send to twitter to do the authentication
    app.get('/auth/twitter', passport.authenticate('twitter', {
        scope: 'email'
    }));

    // handle the callback after twitter has authenticated the user
    app.get('/auth/twitter/callback',
        passport.authenticate('twitter', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));


    // google ---------------------------------

    // send to google to do the authentication
    app.get('/auth/google', passport.authenticate('google', {
        scope: ['profile', 'email']
    }));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));

    // =============================================================================
    // AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
    // =============================================================================

    // locally --------------------------------
    app.get('/connect/local', function(req, res) {
        res.render('browse?p=0', {
            message: req.flash('loginMessage')
        });
    });
    app.post('/connect/local', passport.authenticate('local-signup', {
        successRedirect: '/profile', // redirect to the secure profile section
        failureRedirect: '/connect/local', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    // facebook -------------------------------

    // send to facebook to do the authentication
    app.get('/connect/facebook', passport.authorize('facebook', {
        scope: 'email'
    }));

    // handle the callback after facebook has authorized the user
    app.get('/connect/facebook/callback',
        passport.authorize('facebook', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));

    // twitter --------------------------------

    // send to twitter to do the authentication
    app.get('/connect/twitter', passport.authorize('twitter', {
        scope: 'email'
    }));

    // handle the callback after twitter has authorized the user
    app.get('/connect/twitter/callback',
        passport.authorize('twitter', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));


    // google ---------------------------------

    // send to google to do the authentication
    app.get('/connect/google', passport.authorize('google', {
        scope: ['profile', 'email']
    }));

    // the callback after google has authorized the user
    app.get('/connect/google/callback',
        passport.authorize('google', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));

    // =============================================================================
    // UNLINK ACCOUNTS =============================================================
    // =============================================================================
    // used to unlink accounts. for social accounts, just remove the token
    // for local account, remove email and password
    // user account will stay active in case they want to reconnect in the future

    // local -----------------------------------
    app.get('/unlink/local', function(req, res) {
        var user = req.user;
        user.local.email = undefined;
        user.local.password = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });

    // facebook -------------------------------
    app.get('/unlink/facebook', function(req, res) {
        var user = req.user;
        user.facebook.token = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });

    // twitter --------------------------------
    app.get('/unlink/twitter', function(req, res) {
        var user = req.user;
        user.twitter.token = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });

    // google ---------------------------------
    app.get('/unlink/google', function(req, res) {
        var user = req.user;
        user.google.token = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });
    return app;

};