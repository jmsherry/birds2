// load the things we need
var mongoose = require('mongoose'),
    mongooseTimes = require("mongoose-times"),
    ObjectId = mongoose.Schema.Types.ObjectId,
    locationSchema;

// var positionSchema = mongoose.Schema({
//     Lat: Number,
//     Long: Number
// });
// var Position = module.exports.position = mongoose.model('Position', positionSchema);

locationSchema = mongoose.Schema({
    position: mongoose.Schema.Types.Mixed,
    placename: String,
    site: String,
    information: String,
    rating: Number,
    ratings: Array,
    spots: [{
        type: ObjectId,
        ref: 'Sighting'
    }]

});

locationSchema.plugin(mongooseTimes);


module.exports.location = mongoose.model('Location', locationSchema);