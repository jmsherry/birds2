define(["BirdsApp"],
    function(BirdsApp) {
        BirdsApp.module("AuthApp.Profile.setupView", function(setupView, BirdsApp, Backbone, Marionette, $, _) {
            setupView.UserProfile = Marionette.ItemView.extend({

                el: "#profile-info",



                connectLocal: function() {
                    BirdsApp.trigger('connect:local');
                },
                connectFacebook: function() {
                    BirdsApp.trigger('connect:facebook');
                },
                connectTwitter: function() {
                    BirdsApp.trigger('connect:twitter');
                },
                connectGoogle: function() {
                    BirdsApp.trigger('connect:google');
                },
                unlinkLocal: function() {
                    BirdsApp.trigger('unlink:local');
                },
                unlinkFacebook: function() {
                    BirdsApp.trigger('unlink:facebook');
                },
                unlinkTwitter: function() {
                    BirdsApp.trigger('unlink:twitter');
                },
                unlinkGoogle: function() {
                    BirdsApp.trigger('unlink:google');
                },

                events: {
                    'click.setup #connectLocal': 'connectLocal',
                    'click.setup #unlinkLocal': 'unlinkLocal',
                    'click.setup #connectFacebook': 'connectFacebook',
                    'click.setup #unlinkFacebook': 'unlinkFacebook',
                    'click.setup #connectTwitter': 'connectTwitter',
                    'click.setup #unlinkTwitter': 'unlinkTwitter',
                    'click.setup #connectGoogle': 'connectGoogle',
                    'click.setup #unlinkGoogle': 'unlinkGoogle',
                },

                render: function() {
                    this.BindUIElements();
                }

            });

        });
        return BirdsApp.AuthApp.Profile.setupView;
    });