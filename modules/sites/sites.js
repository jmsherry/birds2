var Mongoose = require('mongoose'),
    Site = Mongoose.model('Location');

module.exports = function(app, db) {

function rate(req, res) {
        //app.get('handleAuthentication')(req, res);

        Site.findOne({
            _id: req.params.id
        }, function(err, site) {
            console.log(site, typeof site, site.placename);
            var ratings = site.ratings,
                i, length,
                total = 0,
                rating, newRating, userID, newScore;

            if (req.user) {
                userID = Mongoose.Types.ObjectId(req.user.id);
            } else {
                userID = 'Anonymous';
            }

            console.log(typeof ratings);
            console.log('id: ', req.params.id);
            console.log('rating: ', req.params.rating, typeof req.params.rating);


            newScore = parseInt(req.params.rating, 10);
            console.log('newScore: ', newScore, typeof newScore);

            newRating = {
                user: userID,
                comment: "",
                score: newScore
            };

            ratings.push(newRating);
            length = ratings.length;

            for (i = 0; i < length; i += 1) {
                total += ratings[i].score;
            }


            site.rating = Math.floor(total / length);
            console.log('new site.rating: ', site.rating);
            site.ratings = ratings;

            console.log('rating pre save:', site.rating, typeof site.rating);

            site.save(function(err, site) {
                console.log('in callback');
                if (err)
                    console.log(err);

                else
                    console.log('site saved', site);
                res.redirect('/sites');
            });
        });
    }


    app.get('/sites', function(req, res) {
        var sites, renderVars;
        sites = Site.find({}).lean().populate({
            path: 'spots'
        }).exec(function(error, sites) {

            var opts = {
                path: 'spots.bird',
                model: 'Bird'
            };

            Site.populate(sites, opts, function(err, sites) {
                //console.log(sites);
                renderVars = {
                    'sites': sites,
                    'authenticated': req.isAuthenticated(),
                    'appviews': app.get('appviews'),
                    'thisView': 'sites'
                };
                renderVars = app.get('getRenderVars')(renderVars);
                //console.log(renderVars);
                res.render('sites', renderVars);
            });

        });
    });

    //REST ROUTES
    app.get('/data/sites', function(req, res) {
        console.log('in data/sites GET');
        var sites, renderVars;
        sites = Site.find({}).populate('spots').exec(function(error, sites) {
            console.log(arguments);
            if (error) {
                res.send(error);
            }
            res.send(sites);
        });
    });

    app.get('/sites/rate/:id/:rating', function(req, res){
        rate(req, res);
    });
    app.post('/sites/rate/:id/:rating', function(req, res){
        rate(req, res);
    });

// Create / Update
    app.put('/data/sites', function(req, res) {
        console.log('in data/sites put');
        console.log(req.body);
        // var sites, renderVars;
        // sites = Site.find({}).populate('spots').exec(function(error, sites) {
        //     console.log(arguments);
        //     if (error) {
        //         res.send(error);
        //     }
        //     res.send(sites);
        // });
    });

// Delete
    app.delete('/data/sites', function(req, res) {
        console.log('in data/sites delete');
        console.log(req.body);
        // var sites, renderVars;
        // sites = Site.find({}).populate('spots').exec(function(error, sites) {
        //     console.log(arguments);
        //     if (error) {
        //         res.send(error);
        //     }
        //     res.send(sites);
        // });
    });

    return app;
};