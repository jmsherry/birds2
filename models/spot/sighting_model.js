// load the things we need
var mongoose = require("mongoose"),
    mongooseTimes = require("mongoose-times"),
    ObjectId = mongoose.Schema.Types.ObjectId,
    sightingSchema;

// define the schema for our user model
sightingSchema = mongoose.Schema({
    "user": {
        "type": ObjectId,
        "ref": "User"
    },
    "bird": {
        "type": ObjectId,
        "ref": "Bird"
    },
    "location": {
        "type": ObjectId,
        "ref": "Location"
    },
    "dateTime": Date
});

sightingSchema.plugin(mongooseTimes);

// create the model for users and expose it to our app
module.exports = mongoose.model("Sighting", sightingSchema);