var CreateCommonDustjsHelpers = function() {
    "use strict";

    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    };

    function CommonDustjsHelpers() {
        // this._inner_if_helper = __bind(this._inner_if_helper, this);
        // this.unless_helper = __bind(this.unless_helper, this);
        // this.if_helper = __bind(this.if_helper, this);
        // this.count_helper = __bind(this.count_helper, this);
        // this.even_helper = __bind(this.even_helper, this);
        // this.odd_helper = __bind(this.odd_helper, this);
        // this.last_helper = __bind(this.last_helper, this);
        // this.first_helper = __bind(this.first_helper, this);
        this.downcase_helper = __bind(this.downcase_helper, this);
        this.titlecase_helper = __bind(this.titlecase_helper, this);
        this.upcase_helper = __bind(this.upcase_helper, this);
        // this.repeat_helper = __bind(this.repeat_helper, this);
        // this.filter_helper = __bind(this.filter_helper, this);
        this.despace_helper = __bind(this.despace_helper, this);
        this.respace_helper = __bind(this.respace_helper, this);
        this.get_helpers = __bind(this.get_helpers, this);
        this.export_helpers_to = __bind(this.export_helpers_to, this);
    }

    CommonDustjsHelpers.dust = null;


    CommonDustjsHelpers.prototype.export_helpers_to = function(dust) {
        dust.helpers = this.get_helpers(dust.helpers);
        CommonDustjsHelpers.dust = dust;
        return CommonDustjsHelpers;
    };

    CommonDustjsHelpers.prototype.get_helpers = function(helpers) {
        if (helpers === null) {
            helpers = {};
        }
        // helpers['if'] = this.if_helper;
        // helpers.unless = this.unless_helper;
        helpers.upcase = helpers.UPCASE = this.upcase_helper;
        helpers.downcase = this.downcase_helper;
        helpers.titlecase = helpers.Titlecase = this.titlecase_helper;
        // helpers.filter = this.filter_helper;
        // helpers.count = this.count_helper;
        // helpers.repeat = this.repeat_helper;
        // helpers.first = this.first_helper;
        // helpers.last = this.last_helper;
        // helpers.even = this.even_helper;
        // helpers.odd = this.odd_helper;
        helpers.respace = this.respace_helper;
        helpers.despace = this.despace_helper;
        // if (helpers.sep === null) {
        //     helpers.sep = this.classic_sep;
        // }
        // if (helpers.idx === null) {
        //     helpers.idx = this.classic_idx;
        // }
        return helpers;
    };

    // CommonDustjsHelpers.prototype._eval_dust_string = function(str, chunk, context) {
    //     var buf;
    //     if (typeof str === "function") {
    //         if (str.length === 0) {
    //             str = str();
    //         } else {
    //             buf = '';
    //             (chunk.tap(function(data) {
    //                 buf += data;
    //                 return '';
    //             })).render(str, context).untap();
    //             str = buf;
    //         }
    //     }
    //     return str;
    // };

    // CommonDustjsHelpers.prototype.classic_idx = function(chunk, context, bodies) {
    //     return bodies.block(chunk, context.push(context.stack.index));
    // };

    // CommonDustjsHelpers.prototype.classic_sep = function(chunk, context, bodies) {
    //     if (context.stack.index === context.stack.of - 1) {
    //         return chunk;
    //     }
    //     return bodies.block(chunk, context);
    // };

    // CommonDustjsHelpers.prototype._render_if_else = function(b, chunk, context, bodies, params) {
    //     if (b === true) {
    //         if (bodies.block !== null) {
    //             chunk = chunk.render(bodies.block, context);
    //         }
    //     } else {
    //         if (bodies["else"] !== null) {
    //             chunk = chunk.render(bodies["else"], context);
    //         }
    //     }
    //     return chunk;
    // };

    // CommonDustjsHelpers.prototype.filter_helper = function(chunk, context, bodies, params) {
    //     var filter_type;
    //     if ((params !== null ? params.type : 'undefined'
    //         0) !== null) {
    //         filter_type = this._eval_dust_string(params.type, chunk, context);
    //     }
    //     return chunk.capture(bodies.block, context, function(data, chunk) {
    //         if (filter_type != null) {
    //             data = CommonDustjsHelpers.dust.filters[filter_type](data);
    //         }
    //         chunk.write(data);
    //         return chunk.end();
    //     });
    // };

    // CommonDustjsHelpers.prototype.repeat_helper = function(chunk, context, bodies, params) {
    //     var i, times, _i, _ref, _ref1, _ref2, _ref3;
    //     times = parseInt(this._eval_dust_string(params.times, chunk, context));
    //     if ((times != null) && !isNaN(times)) {
    //         if ((_ref = context.stack.head) != null) {
    //             _ref['$len'] = times;
    //         }
    //         for (i = _i = 0; 0 <= times ? _i < times : _i > times; i = 0 <= times ? ++_i : --_i) {
    //             if ((_ref1 = context.stack.head) != null) {
    //                 _ref1['$idx'] = i;
    //             }
    //             chunk = bodies.block(chunk, context.push(i, i, times));
    //         }
    //         if ((_ref2 = context.stack.head) != null) {
    //             _ref2['$idx'] = void 0;
    //         }
    //         if ((_ref3 = context.stack.head) != null) {
    //             _ref3['$len'] = void 0;
    //         }
    //     }
    //     return chunk;
    // };

    CommonDustjsHelpers.prototype.upcase_helper = function(chunk, context, bodies, params) {
        return chunk.capture(bodies.block, context, function(data, chunk) {
            chunk.write(data.toUpperCase());
            return chunk.end();
        });
    };

    CommonDustjsHelpers.prototype.titlecase_helper = function(chunk, context, bodies, params) {
        //console.log('titlecase helper called');
        return chunk.capture(bodies.block, context, function(data, chunk) {
            chunk.write(data.replace(/([^\W_]+[^\s-]*) */g, (function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1);
            })));
            return chunk.end();
        });
    };

    CommonDustjsHelpers.prototype.downcase_helper = function(chunk, context, bodies, params) {
        return chunk.capture(bodies.block, context, function(data, chunk) {
            chunk.write(data.toLowerCase());
            return chunk.end();
        });
    };

    // CommonDustjsHelpers.prototype.first_helper = function(chunk, context, bodies, params) {
    //     var c, _ref;
    //     if ((context != null ? (_ref = context.stack) != null ? _ref.index : void 0 : void 0) != null) {
    //         c = context.stack.index === 0;
    //         return this._render_if_else(c, chunk, context, bodies, params);
    //     }
    //     return chunk;
    // };

    // CommonDustjsHelpers.prototype.last_helper = function(chunk, context, bodies, params) {
    //     var c, _ref;
    //     if ((context != null ? (_ref = context.stack) != null ? _ref.index : void 0 : void 0) != null) {
    //         c = context.stack.index === (context.stack.of - 1);
    //         return this._render_if_else(c, chunk, context, bodies, params);
    //     }
    //     return chunk;
    // };

    // CommonDustjsHelpers.prototype.odd_helper = function(chunk, context, bodies, params) {
    //     var c, _ref;
    //     if ((context != null ? (_ref = context.stack) != null ? _ref.index : void 0 : void 0) != null) {
    //         c = context.stack.index % 2 === 1;
    //         return this._render_if_else(c, chunk, context, bodies, params);
    //     }
    //     return chunk;
    // };

    // CommonDustjsHelpers.prototype.even_helper = function(chunk, context, bodies, params) {
    //     var c, _ref;
    //     if ((context != null ? (_ref = context.stack) != null ? _ref.index : void 0 : void 0) != null) {
    //         c = context.stack.index % 2 === 0;
    //         return this._render_if_else(c, chunk, context, bodies, params);
    //     }
    //     return chunk;
    // };

    // CommonDustjsHelpers.prototype.count_helper = function(chunk, context, bodies, params) {
    //     var value;
    //     value = this._eval_dust_string(params.of, chunk, context);
    //     if ((value != null ? value.length : void 0) != null) {
    //         chunk.write(value.length);
    //     }
    //     return chunk;
    // };

    // CommonDustjsHelpers.prototype.if_helper = function(chunk, context, bodies, params) {
    //     var execute_body;
    //     execute_body = this._inner_if_helper(chunk, context, bodies, params);
    //     return this._render_if_else(execute_body, chunk, context, bodies, params);
    // };

    // CommonDustjsHelpers.prototype.unless_helper = function(chunk, context, bodies, params) {
    //     var execute_body;
    //     execute_body = this._inner_if_helper(chunk, context, bodies, params);
    //     execute_body = !execute_body;
    //     return this._render_if_else(execute_body, chunk, context, bodies, params);
    // };

    CommonDustjsHelpers.prototype.despace_helper = function(chunk, context, bodies, params) {
        return chunk.capture(bodies.block, context, function(data, chunk) {
            chunk.write(encodeURIComponent(data));
            return chunk.end();
        });
    };

    CommonDustjsHelpers.prototype.respace_helper = function(chunk, context, bodies, params) {
        return chunk.capture(bodies.block, context, function(data, chunk) {
            chunk.write(decodeURIComponent(data));
            return chunk.end();
        });
    };

    // CommonDustjsHelpers.prototype._inner_if_helper = function(chunk, context, bodies, params) {
    //     var above, below, c, countof, execute_body, isntval, isval, matches, re, value, _i, _len, _ref;
    //     execute_body = false;
    //     if (params != null) {
    //         if (params.test != null) {
    //             value = this._eval_dust_string(params.test, chunk, context);
    //         }
    //         _ref = ['count', 'count_of', 'count-of', 'countof'];
    //         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    //             c = _ref[_i];
    //             if (params[c] != null) {
    //                 countof = this._eval_dust_string(params[c], chunk, context);
    //                 if ((countof != null ? countof.length : void 0) != null) {
    //                     value = countof.length;
    //                 }
    //             }
    //         }
    //         if (value == null) {
    //             value = this._eval_dust_string(params.value, chunk, context);
    //         }
    //         if (value != null) {
    //             if (("" + value) === ("" + (parseFloat(value)))) {
    //                 value = parseFloat(value);
    //             }
    //             if (params.matches != null) {
    //                 matches = this._eval_dust_string(params.matches, chunk, context);
    //                 re = new RegExp(matches);
    //                 execute_body = re.test(value);
    //             } else if (params['is'] != null) {
    //                 isval = this._eval_dust_string(params['is'], chunk, context);
    //                 if (typeof value === 'number' && (!isNaN(parseFloat(isval)))) {
    //                     isval = parseFloat(isval);
    //                 }
    //                 execute_body = value === isval;
    //             } else if (params['isnt'] != null) {
    //                 isntval = this._eval_dust_string(params['isnt'], chunk, context);
    //                 if (typeof value === 'number' && (!isNaN(parseFloat(isntval)))) {
    //                     isntval = parseFloat(isntval);
    //                 }
    //                 execute_body = value !== isntval;
    //             } else if (params.above != null) {
    //                 above = this._eval_dust_string(params.above, chunk, context);
    //                 if (typeof value === 'number' && (!isNaN(parseFloat(above)))) {
    //                     above = parseFloat(above);
    //                 }
    //                 execute_body = value > above;
    //             } else if (params.below != null) {
    //                 below = this._eval_dust_string(params.below, chunk, context);
    //                 if (typeof value === 'number' && (!isNaN(parseFloat(below)))) {
    //                     below = parseFloat(below);
    //                 }
    //                 execute_body = value < below;
    //             } else {
    //                 execute_body = value === true || value === 'true' || value === 'TRUE' || value === 't' || value === 'T' || value === 1 || value === '1' || value === 'on' || value === 'ON' || value === 'yes' || value === 'YES' || value === 'y' || value === 'Y';
    //             }
    //         }
    //     }
    //     return execute_body;
    // };

    return CommonDustjsHelpers;
}();


var cdjsHelpers = new CreateCommonDustjsHelpers(); //executes class to create obj


module.exports.cdjsHelpers = cdjsHelpers;