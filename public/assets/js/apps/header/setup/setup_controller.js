define(["BirdsApp", "apps/header/setup/setup_view"], function(BirdsApp, View) {
    BirdsApp.module("HeaderApp.Setup", function(Setup, BirdsApp, Backbone, Marionette, $, _) {
        Setup.Controller = {
            setup: function() {

                var brand = new View.Brand();
                brand.on('brand:clicked', function() {
                    console.log('brand:clicked');
                    BirdsApp.trigger('browse:list');
                });

                var mainNav = new View.mainNav();
            },

            setActiveHeader: function(headerUrl) {
                require(["entities/header", "common/views"], function(header, CommonViews) {
                    var links = BirdsApp.request("header:entities");
                    var headerToSelect = links.find(function(header) {
                        return header.get("name") === headerUrl;
                    });
                    headerToSelect.select();
                    links.trigger("reset");
                });
            },

            deselect: function() {
                $('#main-nav').find('.active').removeClass('active');
            }
        };
    });

    return BirdsApp.HeaderApp.Setup.Controller;
});