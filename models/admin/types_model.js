// load the things we need
var mongoose = require('mongoose'),
    mongooseTimes = require("mongoose-times");

// define the schema for our user model
var typesSchema = mongoose.Schema({
    type: mongoose.Schema.Types.Mixed,
    subtypes: mongoose.Schema.Types.Mixed
});

typesSchema.plugin(mongooseTimes);

// create the model for users and expose it to our app
module.exports = mongoose.model('Types', typesSchema);