define(["BirdsApp", "apps/browse/list/list_view"], function(BirdsApp, View) {
    BirdsApp.module("Browse.List", function(List, BirdsApp, Backbone, Marionette, $, _) {
        List.Controller = {
            listBirds: function(options) {
                console.log('received options: ', arguments);
                require(["entities/bird", "common/views"], function(bird, CommonViews) {
                    //console.log('HERE: ', arguments);

                    var spinner = new CommonViews.Loading();
                    BirdsApp.content.show(spinner);


                    function processURL(url) {
                        //var options = {};
                        url = url.split('=');
                        options.page = parseInt(url[1], 10);
                        url = url[0].split('?');
                        url = url[0].split('/');
                        console.log(url);
                        options.type = url[2] ? url[2].replace(/_/g, " ") : url[2];
                        options.subtype = url[3] ? url[3].replace(/_/g, " ") : url[3];
                        options.name = url[4] ? url[4].replace(/_/g, " ") : url[4];

                        if (options.name) {
                            options.searchCriterion = options.name;
                            options.soloView = true;
                        } else if (options.subtype) {
                            options.searchCriterion = options.subtype;
                        } else if (options.type) {
                            options.searchCriterion = options.type;
                        }
                        console.log(options);
                    }

                    processURL(options.url);

                    var breadcrumbs = new View.breadcrumbs(options);
                    BirdsApp.breadcrumbs.show(breadcrumbs);

                    //console.log('Marionette: ', Marionette);

                    var fetchingbirds = BirdsApp.request("bird:entities");
                    require(["entities/common"], function(FilteredCollection) {
                        $.when(fetchingbirds).done(function(birds) {

                            console.log('in the fetchingbirds callback: ', birds, birds.length, typeof birds, ' options: ', options);

                            var standardOptions, pageTotals, startPoint, endPoint, displayLimit, resultLength, page, listLayout,
                                birdsListView, pagination, paginationNext, paginationPrev, prevURL, nextURL, newBirds, finalBirds,
                                itemViewOptions, prevPage, nextPage;

                            standardOptions = {
                                displayLimit: 9
                            };

                            options = _.extend(standardOptions, options);

                            if (options.page === undefined || isNaN(options.page)) {
                                options.page = 0;
                            }

                            console.log('NEW OPTIONS: ', options);

                            //default settings
                            pagination = false;
                            paginationPrev = false;
                            paginationNext = false;


                            var filteredBirds = BirdsApp.Entities.FilteredCollection({
                                collection: birds,
                                filterFunction: function(filterCriterion) {
                                    return function(bird) {
                                        if (bird.get('name').toLowerCase().indexOf(filterCriterion) !== -1 || bird.get('subtype').toLowerCase().indexOf(filterCriterion) !== -1 || bird.get('type').toLowerCase().indexOf(filterCriterion) !== -1) {
                                            return bird;
                                        }
                                    };
                                }
                            });

                            if (options.searchCriterion) {
                                filteredBirds.filter(options.searchCriterion);
                                BirdsApp.content.once("show", function() {
                                    BirdsApp.triggerMethod("set:filter:criterion", options);
                                });
                            }


                            displayLimit = options.displayLimit;
                            page = options.page;
                            prevPage = page - 1;
                            nextPage = page + 1;
                            startPoint = page * displayLimit;
                            endPoint = startPoint + displayLimit;
                            resultLength = filteredBirds.length;

                            if (resultLength > displayLimit) {
                                pagination = true;
                                if (startPoint > 0) {
                                    paginationPrev = true;
                                    prevURL = 'browse?p=' + prevPage;
                                }

                                if (endPoint < resultLength) {
                                    paginationNext = true;
                                    nextURL = 'browse?p=' + nextPage;
                                }
                            }

                            newBirds = filteredBirds.slice(startPoint, endPoint);

                            //Adjust for zero basing
                            startPoint += 1;

                            if (endPoint >= resultLength) {
                                if (startPoint === resultLength) {
                                    pageTotals = startPoint + ' of ' + resultLength;
                                } else {
                                    pageTotals = startPoint + ' to ' + resultLength + ' of ' + resultLength;
                                }
                            } else {
                                pageTotals = startPoint + ' to ' + endPoint + ' of ' + resultLength;
                            }

                            console.log('newBirds slice: ', newBirds, typeof newBirds);

                            birds.reset(newBirds);

                            //console.log('finalBirds: ', finalBirds, typeof finalBirds);

                            birdsListView = new View.birds({
                                collection: birds,
                                itemViewOptions: options
                            });
                            console.log('birdsListView', birdsListView);


                            itemViewOptions = {
                                pagination: pagination,
                                paginationNext: paginationNext,
                                paginationPrev: paginationPrev,
                                nextURL: nextURL,
                                prevURL: prevURL,
                                page: options.page,
                                displayLimit: displayLimit,
                                startPoint: startPoint,
                                endPoint: endPoint,
                                resultLength: resultLength,
                                pageTotals: pageTotals
                            };

                            console.log(itemViewOptions);

                            pagination = new View.pagination(itemViewOptions);
                            console.log('pagination: ', pagination);

                            listLayout = new View.listLayout();
                            console.log('listLayout: ', listLayout);

                            listLayout.on("show", function() {
                                listLayout.paginationRegion.show(pagination);
                                listLayout.listRegion.show(birdsListView);
                            });

                            BirdsApp.content.show(listLayout);
                        }); //when
                    }); //req
                }); // req entit common
            }
        };
    });

    return BirdsApp.Browse.List.Controller;
});