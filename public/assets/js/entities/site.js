define([
    'BirdsApp',
    'apps/config/storage/localstorage'
], function(BirdsApp) {
    //console.log('entityBirdsArgs: ', arguments);
    BirdsApp.module("Entities", function(Entities, BirdsApp, Backbone, Marionette, $, _) {

        Entities.Site = Backbone.Model.extend({

            urlRoot: "data/sites",
            idAttribute: "_id",
            ajaxSync: Backbone.ajaxSync,
            defaults: {

            },
            initialize: function() {

            },
            validate: function(attrs, options) {

            }

        });

        Entities.configureStorage(Entities.Site);

        Entities.SiteCollection = Backbone.Collection.extend({
            model: Entities.Site,
            comparator: "name",
            ajaxSync: Backbone.ajaxSync,
            url: "data/sites"
        });

        Entities.configureStorage(Entities.SiteCollection);



        var API = {
            getSiteEntities: function() {
                console.log('AJAX GET SITES');
                var sites = new Entities.SiteCollection(),
                    defer = $.Deferred(),
                    storedSites = localStorage.getItem('data/sites');

                if (storedSites === null || !BirdsApp.updated.sites && Offline.state === 'up') {
                    //Fetch via Ajax
                    sites.ajaxSync("read", sites, {
                        success: function(data) {
                            _.forEach(data, function(site) {
                                var thisSite = new Entities.Site(site);
                                sites.add(thisSite);
                            });

                            sites.forEach(function(site) {
                                site.save();
                            });

                            BirdsApp.updated.sites = true;

                            defer.resolve(sites);
                        },
                        error: function(err) {
                            console.log(err);
                            alert('Error: ' + err.status + ' ' + err.statusText + '\n' + err.responseText + ' \nPlease refresh the page or contact the maintainer');
                        }
                    });

                } else {

                    console.log('LS GET SITES');
                    //Fetch from local Storage
                    sites.fetch({
                        success: function(collection, response, options) {
                            console.log('fetched sites: ', collection.length, arguments);
                            defer.resolve(collection);
                        },
                        error: function(collection, response, options) {
                            console.log('error: ', arguments);
                        }
                    });



                }

                return defer.promise();

            }
        };

        BirdsApp.reqres.setHandler("site:entities", function() {
            return API.getSiteEntities();
        });

    });

    return;
});