var mongoose = require('mongoose'),
    moment = require('moment'),
    Bird = mongoose.model('Bird'),
    Sighting = mongoose.model('Sighting'),
    User = mongoose.model('User'),
    locationModel = mongoose.model('Location'),
    Backbone = require('backbone');


module.exports = function(app, db) {

    app.get('/spot/:name?', function(req, res) {
        app.get('handleAuthentication')(req, res);

        console.log('in spot');
        var birds, locations, renderVars, bird;

        bird = req.params.name;

        function callback(err, birds) {
            locations = locationModel.find({}, function(err, locations) {

                renderVars = {
                    'authenticated': req.isAuthenticated(),
                    'loginForm': true,
                    'thisView': 'spot',
                    'locations': locations
                };

                if (birds && birds.length === 1) {
                    renderVars.bird = birds[0];
                } else if (birds) {
                    renderVars.birds = birds;
                    renderVars.bird = null;
                } else {
                    throw new Error('No_birds');
                }
                renderVars = app.get('getRenderVars')(renderVars);

                res.render('spot', renderVars);

            });

        }

        if (bird) {
            bird = app.get('space')(bird);
            searchTerm = {
                name: bird
            };

        } else {

            searchTerm = {};
        }

        birds = Bird.find(searchTerm, callback);

    });

    app.get('/spots', function(req, res) {
        app.get('handleAuthentication')(req, res);

        //console.log('in spot');
        var sightings, userid, renderVars;
        userid = req.user._id;
        Sighting.find({
            'user': userid
        }).populate({
            path: 'user bird location'
        }).exec(function(err, sightings) {

            //console.log('original sightings: ', sightings);

            renderVars = {
                'authenticated': req.isAuthenticated(),
                'loginForm': true,
                'appviews': app.get('appviews'),
                'spots': sightings,
                'thisView': 'spot',
                'messages': req.flash()
            };
            renderVars = app.get('getRenderVars')(renderVars);
            res.render('spots', renderVars);

        });
    });

    function spot(req, res) {
        app.get('handleAuthentication')(req, res);

        //console.log('inside spot: ', req.params.id);

        var postVals = req.body,
            user = req.user._id,
            bird = postVals.bird,
            dateTime,
            newSighting,
            updateObj,
            location = postVals.site,
            changeCurrentLocation = postVals.currentLocation,
            isAjax;

            req.xhr ? isAjax = true : isAjax = false;

        if(isAjax){
            dateTime = moment(postVals.dateTime, 'MM/DD/YYYY HH:mm');
        } else {
            dateTime = moment(postVals.year + '-' + postVals.month + '-' + postVals.date + ' ' + postVals.hour + ':' + postVals.minute, "YYYY-MMM-DD HH:mm");
        }
        console.log('dateTime: ', dateTime, dateTime.isValid());

        if (dateTime.isValid() === false) {
            req.flash('error', 'Date set incorrectly. Please try again');
            if(isAjax){
                return res.json(new Error('Malformed Date'));
            } else {
                res.redirect('/spots');
            }
        } else if (dateTime.isAfter()){
            req.flash('error', 'The date you specified is in the future');
            if(isAjax){
                return res.json(new Error('Future Date'));
            } else {
                res.redirect('/spots');
            }
        }

        //console.log('user: ', user, ' bird: ', bird, 'postVals: ', postVals);
        if (changeCurrentLocation) {
            User.update({
                _id: user
            }, {
                $set: {
                    currentLocation: location
                }
            }, function(err, user) {
                if (err) {
                    return console.error(err);
                }
                console.log('updated user currentLocation', user);
            });
        }

        updateObj = {};
        updateObj = {
            'user': user,
            'bird': bird,
            'location': location,
            'dateTime': dateTime.format()
        };

        console.log(updateObj.dateTime);

        if (!updateObj.bird) {
            req.flash('error', 'Please select a bird from the list');
        } else if (!updateObj.location) {
            req.flash('error', 'Please select a location from the list');
        }

        if (req.params.id) {
            //console.log('updating');
            Sighting.update({
                _id: req.params.id
            }, {
                $set: updateObj
            }, function(err, spot) {
                console.log('arguments: ', arguments);
                if (err) return console.error(err);
                console.log('Sighting %s updated: ', req.params.id);
                req.flash('info', 'Sighting updated!');
                if(req.xhr){
                    console.log('recognising ajax');
                    res.json(spot);
                }
                
            });
        } else {
            //console.log('creating new');
            newSighting = new Sighting(updateObj);

            newSighting.save(function(err, sighting) {
                if (err) return console.error(err);
                //console.log('user', user, 'sighting.user', sighting);
                console.log('New sighting %s saved: ', sighting._id);
                req.flash('info', 'New sighting saved!');
                if(req.xhr){
                    console.log('recognising ajax');
                    res.json(sighting);
                }
            });
        }
        //app.get('updateRankings')();

    }

    app.post('/spot', function(req, res) {
        spot(req, res);
        res.redirect('/spots');
    });

    app.get('/spot/update/:id', function(req, res) {
        app.get('handleAuthentication')(req, res);
        Sighting.findOne({
            _id: req.params.id
        }).populate('bird location').exec(function(err, spot) {
            //console.log(spot);
            locationModel.find({}, function(err, locations) {

                Bird.find({}, function(err, birds) {
                    var renderVars = {
                        birds: birds,
                        sighting: spot,
                        bird: null,
                        locations: locations
                    };
                    renderVars = app.get('getRenderVars')(renderVars);
                    res.render('spot', renderVars);

                });

            });
        });
    });

    app.post('/spot/update/:id', function(req, res) {
        //console.log('inside original route:', req.params.id);
        spot(req, res);
    });

    app.post('/spots', function(req, res) {
        //console.log('inside original route:', req.params.id);
        res/redirect('/spots');
    });

    app.get('/spot/delete/:id', function(req, res) {
        app.get('handleAuthentication')(req, res);
        Sighting.remove({
            _id: req.params.id
        }, function(err) {
            if (err) {
                req.flash('error', 'Something went wrong! Sighting not deleted');
            } else {
                req.flash('info', 'Sighting Deleted');
            }
            res.redirect('/spots');
        });
    });

    //************************************//
    //REST ROUTES ************************//
    //************************************//

    // Create
    app.post('/data/spots', function(req, res) {
        console.log('\n\n in data/spots POST:', req.params);
        spot(req, res);
    });

    //Read
    app.get('/data/spots', function(req, res) {
        console.log('data/spots');
        var callback = function(err, spots) {
            console.log('in data/spots GET callback', arguments);
            if (err) {
                res.send(err);
            }
            res.send(spots);
        };

       // console.log(req.user._id);
        Sighting.find({}).populate({
            path: 'user',
            select: 'user',
            match: {
                _id: req.user._id
            }
        }).populate('bird location user').exec(callback);
    });

    //Update
    app.put('/data/spots/:id', function(req, res) {
        console.log('in data/spots put');
       // console.log(req.params);
        spot(req, res);
    });

    // Delete
    app.delete('/data/spots/:id', function(req, res) {
        console.log('in data/spots delete');
        //console.log('request params', req.params);
        Sighting.findOne({_id: req.params.id}, function(err, spot){
            if (err) res.send(err);
            spot.remove(function(err, numberAffected, rawResponse){
                if (err) {
                    req.flash('error', 'Something went wrong! Sighting not deleted');
                } else {
                    req.flash('info', 'Sighting Deleted');
                }
                //console.log('rawResponse', rawResponse);
                console.log('numberAffected', numberAffected);
                res.json(numberAffected);
            });
        });
    });

    return app;
};