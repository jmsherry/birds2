define(["BirdsApp", "apps/typeLinks/setup/setup_view"], function(BirdsApp, View) {
    BirdsApp.module("TypeLinks.Setup", function(Setup, BirdsApp,
        Backbone, Marionette, $, _) {
        Setup.Controller = {
            setupLinks: function(criterion) {

                var typeLinksSetupView = new View.typeLinks({
                    el: '#type-links'
                });

            }
        };
    });

    return BirdsApp.TypeLinks.Setup.Controller;
});