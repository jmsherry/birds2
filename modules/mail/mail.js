module.exports.sendEmail = function(options, type) {
    var nodemailer, _, smtpTransport, mailOptions;
    nodemailer = require("nodemailer");
    _ = require("lodash");

    // create reusable transport method (opens pool of SMTP connections)
    smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: "gmail.user@gmail.com",
            pass: "userpass"
        }
    });

    mailOptions = {};

    switch (type) {
        case "signup":
            // setup e-mail data with unicode symbols
            mailOptions = {
                from: "accounts@birdsapp.heroku.com", // sender address
                to: options.addresses, // list of receivers
                subject: "Account Confirmation for BirdsApp", // Subject line
                text: "Thanks for joining BirdsApp. Please visit the following address to confirm your account: " + options.confirmURL, // plaintext body
                html: "<h1>Thanks for joining BirdsApp</h1><p>Please click the following link to confirm your account:  <a href = \"" + options.confirmURL + "\">Link</a></p>", // html body
            };
            break;
    }


    mailOptions = _.extend(mailOptions, options);

    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: " + response.message);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });
};