define(["BirdsApp", "apps/sites/list/list_view"], function(BirdsApp, View) {
    BirdsApp.module("Sites.List", function(List, BirdsApp,
        Backbone, Marionette, $, _) {
        List.Controller = {
            list: function() {

                var itemViewOptions = {};

                require(["entities/site"], function() {

                    var fetchingsites = BirdsApp.request("site:entities");

                    $.when(fetchingsites).done(function(sites) {
                        console.log('done: ', sites);
                        var sitesMapView = new View.sites({
                            collection: sites,
                            itemViewOptions: itemViewOptions
                        });

                        BirdsApp.content.show(sitesMapView);
                    });
                });
            }
        };
    });

    return BirdsApp.Sites.List.Controller;
});