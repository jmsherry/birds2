var mongoose = require('mongoose'),
    mongooseTimes = require("mongoose-times"),
    ObjectId = mongoose.Schema.Types.ObjectId,

    commentSchema = mongoose.Schema({

        comment: String,
        source: String,
        user: {
            type: ObjectId,
            ref: 'User'
        }

    });

commentSchema.plugin(mongooseTimes);

// create the model for users and expose it to our app
module.exports = mongoose.model('Comment', commentSchema);