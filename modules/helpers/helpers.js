module.exports = function(app, _) {
    app.set('getRenderVars', function(vars) {
        var renderVars = _.extend(app.get('baseview'), vars);
        return renderVars;
    });

    app.set("handleAuthentication", function(req, res) {
        console.log('handleAuthentication. Logged in; ', req.isAuthenticated());
        if (!req.isAuthenticated()) {
            res.redirect('/browse?p=0');
        }
    });

    app.set('encode', function(str) {
        // console.log('in space function, was passed: ', str);
        if (str) {
            str = encodeURIComponent(str);
            return str;
        } else {
            str = null;
        }
        return str;
    });

    app.set('decode', function(str) {
        //console.log('in despace function, was passed: ', str);
        if (str) {
            str = decodeURIComponent(str);
            return str;
        } else {
            str = null;
        }
        return str;
    });

    app.set('searchTerm', function(name, subtype, type) {
        //console.log('in searchTerm', arguments);
        var searchTerm = {};
        if (name !== null && name !== undefined) {
            searchTerm.name = name;
        } else if (subtype !== null && subtype !== undefined) {
            searchTerm.subtype = subtype;
        } else if (type !== null && type !== undefined) {
            searchTerm.type = type;
        }
        //console.dir('searchTerm: ', searchTerm);
        return searchTerm;
    });

    app.set('queryStringCheck', function(res, req) {
        //console.log('in queryStringCheck');
        if (req.originalUrl) {
            if (!req.query.p) {
                // console.log('redirecting to: ', req.originalUrl + '?p=0');
                res.redirect(req.originalUrl + '?p=0');
            } else {
                //console.log('returning: ', req.query.p);
                return req.query.p;
            }
        }
    });

    app.set('sortByKey', function(collection, key) {

        var newBirds, names, i, collectionLength, namesLength;
        newBirds = [];
        names = [];
        collectionLength = collection.length;

        // walk original array to map values and positions
        for (i = 0; i < collectionLength; i += 1) {
            names.push({
                // remember the index within the original array
                index: i,
                // evaluate the value to sort
                value: collection[i][key].toLowerCase()
            });
        }
        // sorting the map containing the reduced values
        names.sort(function(a, b) {
            return a.value > b.value ? 1 : -1;
        });

        namesLength = names.length;
        // copy values in right order
        for (i = 0; i < namesLength; i += 1) {
            //console.log(i);
            newBirds.push(collection[names[i].index]);
        }

        //console.log(newBirds);
        return newBirds;
    });

    return app;
};