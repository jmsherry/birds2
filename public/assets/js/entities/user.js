define([
    'BirdsApp',
    'apps/config/storage/localstorage'
], function(BirdsApp) {
    //console.log('entityBirdsArgs: ', arguments);
    BirdsApp.module("Entities", function(Entities, BirdsApp, Backbone, Marionette, $, _) {

        Entities.User = Backbone.Model.extend({
            urlRoot: 'data/users',
            idAttribute: "_id",
            isCurrentUser: false,
            ajaxSync: Backbone.ajaxSync,
            updatePoints: function(){
                //this.attributes
            }
        });
        Entities.configureStorage(Entities.User);

        Entities.UserCollection = Backbone.Collection.extend({
            url: 'data/users',
            ajaxSync: Backbone.ajaxSync,
            getCurrentUser: function() {
                var i, models, length, thisModel;

                models = this.models;
                length = models.length;


                for (i = 0; i < length; i += 1) {
                    thisModel = models[i];
                    if (thisModel.get('isCurrentUser')) {
                        return thisModel;
                    }
                }
                
            }
        });
        Entities.configureStorage(Entities.UserCollection);


        var API = {
            getUserEntities: function() {

                var users = new Entities.UserCollection();
                var defer = $.Deferred();

                var storedUsers = localStorage.getItem('data/users');
                console.log("storedUsers", storedUsers);
                if (storedUsers === null || !BirdsApp.updated.users && Offline.state === 'up') {
                    console.log('ajaxFetch');
                    //Fetch via Ajax
                    users.ajaxSync("read", users, {
                        success: function(userModels) {
                            console.log('success: ', users);

                            _.forEach(userModels, function(user) {
                                var thisUser = new Entities.User(user);
                                users.add(thisUser);
                            });

                            users.forEach(function(user) {
                                user.save();
                            });

                            BirdsApp.updated.users = true;

                            //console.log(users);
                            defer.resolve(users);
                        },
                        error: function(err) {
                            console.log(err);
                            alert('Error: ' + err.status + ' ' + err.statusText + '\n' + err.responseText + ' \nPlease refresh the page or contact the maintainer');
                        }
                    });

                    //return defer.promise();

                } else {

                    console.log('localFetch');

                    //var defer = $.Deferred();

                    //Fetch from local Storage
                    users.fetch({
                        success: function(collection, response, options) {
                            console.log('fetched users: ', collection.length, arguments);
                            defer.resolve(collection);
                        },
                        error: function(collection, response, options) {
                            console.log('error: ', arguments);
                        }
                    });



                }

                return defer.promise();

            }
        };


        BirdsApp.reqres.setHandler("user:entities", function() {
            console.log('user:entities');
            return API.getUserEntities();
        });
        BirdsApp.reqres.setHandler('user:currentUser', function() {
            console.log('user:currentUser');
            console.log(BirdsApp);
            return BirdsApp.currentUser;
        });



    });

});