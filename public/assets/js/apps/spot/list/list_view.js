define([
        "BirdsApp"
    ],
    function(BirdsApp) {
        // console.log('in browse view', arguments);

        BirdsApp.module("Spots.List.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.spot = Backbone.Marionette.ItemView.extend({
                tagName: "tr",
                template: 'spot.dust',
                events: {
                    "click .update-spot": "updateSpot",
                    "click .delete-spot": "deleteSpot"
                },

                getID: function(href){
                    console.log(href);
                    var id = href.split('/');
                    id = id[id.length-1];
                    console.log('id:', id);
                    return id;
                },

                updateSpot: function(e) {
                    console.log(e);
                    var id = this.getID(e.target.attributes.href.value);
                    BirdsApp.trigger("spot:updateForm", id);
                    return false;
                },

                deleteSpot: function(e) {
                    var id = this.getID(e.target.attributes.href.value);
                    BirdsApp.trigger("spot:remove", id, $(e.target).parents('li'));
                    return false;
                },

                initialize: function(){
                    this.listenTo(this.model, "destroy", function() {
                        alert('collection remove');
                        this.$el.remove();
                        this.remove();
                    });
                    this.listenTo(this.model, "change", function() {
                        alert('collection change');
                        this.$el.remove();
                        this.render();
                    });
                }//,
                // serializeData: function(){
                //     console.log(arguments);
                //     console.log(this.model.attributes);
                //     return this.model;
                // }
            });

            View.spots = Backbone.Marionette.CompositeView.extend({
                template: 'spots_list_container.dust',
                childView: View.spot,
                childViewContainer: "tbody",

                events: {
                    "click .add-spot": "addSpot",
                },

                addSpot: function() {
                    BirdsApp.trigger("spots:addForm");
                    return false;
                },

                initialize: function() {
                    console.log('view args: ', arguments);
                    this.listenTo(this.collection, "reset", function() {
                        alert('collection reset');
                        // this.appendHtml = function(collectionView, itemView, index) {
                        //     console.log('target: ', arguments);
                        //     collectionView.$el.append(itemView.el);
                        // };
                    });
                    this.listenTo(this.collection, "add", function() {
                        alert('collection add');
                        this.appendHtml = function(collectionView, itemView, index) {
                            console.log('target: ', arguments);
                            collectionView.$el.append(itemView.el);
                        };
                    });
                    this.listenTo(this.collection, "remove", function() {
                        alert('collection remove');
                        this.appendHtml = function(collectionView, itemView, index) {
                            console.log('collection remove appendHtml: ', arguments);
                            collectionView.$el.append(itemView.el);
                        };
                    });
                    this.listenTo(this.collection, "change", function() {
                        alert('collection change');
                        this.appendHtml = function(collectionView, itemView, index) {
                            console.log('collection change appendHtml: ', arguments);
                            //collectionView.$el.append(itemView.el);
                        };
                    });
                },

                serializeData: function(){
                    return {
                        user: this.options.itemviewOptions.user.attributes
                    };
                }

                // onCompositeCollectionRendered: function() {

                //     this.appendHtml = function(collectionView, itemView, index) {
                //         console.log('oCCR view args: ', arguments);
                //         collectionView.$el.prepend(itemView.el);
                //     };
                // }
            });
        });

        return BirdsApp.Spots.List.View;
    });