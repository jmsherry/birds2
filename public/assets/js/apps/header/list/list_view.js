define(["BirdsApp"],
    function(BirdsApp) {
        BirdsApp.module("HeaderApp.List.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.Header = Marionette.ItemView.extend({
                tagName: 'li',
                template: 'menu_item.dust',

                events: {
                    "click.navigate a": "navigate"
                },

                initialize: function() {
                    //this.$el.off('click.setup');
                },

                navigate: function(e) {
                    
                    console.log('navigating', this.model.attributes);
                    this.model.trigger('itemview:navigate');
                    return false;
                },

                serializeData: function() {
                    var m = this.options.model.attributes;
                    return {
                        "destinationURL": m.destinationURL,
                        "name": m.name
                    };
                },

                onRender: function() {
                    //console.log('header list onRender called');
                    if (this.model.selected) {
                        //console.log('selecting this model: ', this.model);
                        // add class so Bootstrap will highlight the active entry in the navbar
                        this.$el.children().addClass("active");
                    }
                },
                onShow: function() {
                    this.$el.off('click.setup');
                }
            });

            View.Headers = Marionette.CompositeView.extend({
                template: 'main_nav.dust',
                childView: View.Header,
                childViewContainer: "ul",
                serializeData: function() {
                    // console.log(this.options.itemViewOptions);
                    this.options.authenticated = this.options.itemViewOptions.authenticated;
                    //console.log(this);
                }
            });
        });

        return BirdsApp.HeaderApp.List.View;
    });