define([
    'BirdsApp',
    'apps/config/storage/localstorage'
], function(BirdsApp) {
    //console.log('BirdsApp: ', BirdsApp);
    BirdsApp.module("Entities", function(Entities, BirdsApp, Backbone, Marionette, $, _) {

        Entities.Bird = Backbone.Model.extend({

            urlRoot: "data/birds",
            idAttribute: "_id",
            ajaxSync: Backbone.ajaxSync,
            meta: function(prop, value) {
                if (value === undefined) {
                    return this._meta[prop];
                } else {
                    this._meta[prop] = value;
                }
            },
            initialize: function() {
                this._meta = {};
            },
            validate: function(attrs, options) {

            }

        });

        Entities.configureStorage(Entities.Bird);

        Entities.BirdCollection = Backbone.Collection.extend({
            model: Entities.Bird,
            comparator: "name",
            url: "data/birds",
            ajaxSync: Backbone.ajaxSync,
            meta: function(prop, value) {
                if (value === undefined) {
                    return this._meta[prop];
                } else {
                    this._meta[prop] = value;
                }
            },
            initialize: function() {
                this._meta = {};
            }
        });

        Entities.configureStorage(Entities.BirdCollection);


        var API = {

            getBirdEntities: function() {
                var birds = new Entities.BirdCollection(),
                    defer = $.Deferred(),
                    storedBirds = localStorage.getItem('data/birds');

                if (storedBirds === null || !BirdsApp.updated.birds && Offline.state === 'up') {

                    //Fetch via Ajax
                    birds.ajaxSync("read", birds, {
                        success: function(data) {
                            _.forEach(data, function(bird) {
                                var thisBird = new Entities.Bird(bird);
                                birds.add(thisBird);
                            });

                            birds.forEach(function(bird) {
                                bird.save();
                            });

                            BirdsApp.updated.birds = true;

                            defer.resolve(birds);
                        },
                        error: function(err) {
                            console.log(err);
                            alert('Error: ' + err.status + ' ' + err.statusText + '\n' + err.responseText + ' \nPlease refresh the page or contact the maintainer');
                        }
                    });

                } else {

                    //Fetch from local Storage
                    birds.fetch({
                        success: function(collection, response, options) {
                            console.log('fetched birds: ', collection.length, arguments);
                            defer.resolve(collection);
                        },
                        error: function(collection, response, options) {
                            console.log('error: ', arguments);
                        }
                    });

                    //console.log('THIS ONE', birds);
                }
                return defer.promise();

            }
        };

        BirdsApp.reqres.setHandler("bird:entities", function() {
            console.log('bird:entities');
            return API.getBirdEntities();
        });

    });

    return;
});