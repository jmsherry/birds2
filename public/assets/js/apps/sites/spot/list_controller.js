define(["BirdsApp", "apps/bspot/list/list_view"], function(BirdsApp, View) {
    BirdsApp.module("Spots.List", function(List, BirdsApp,
        Backbone, Marionette, $, _) {
        List.Controller = {
            listSpots: function() {

                require(["entities/spot"], function() {

                    var fetchingbirds = BirdsApp.request("spot:entities");

                    //var birdsListLayout = new View.Layout();
                    // //var birdsListPanel = new View.Panel();

                    $.when(fetchingbirds).done(function(birds) {
                        console.log('done: ', birds);
                        birds.sort();
                        var birdsListView = new View.birds({
                            collection: birds
                        });

                        // birdsListLayout.on("show", function() {
                        //     //birdsListLayout.panelRegion.show(birdsListPanel);
                        //     birdsListLayout.birdsRegion.show(birdsListView);
                        // });

                        BirdsApp.content.show(birdsListView);
                    });
                });
            }
        };
    });

    return BirdsApp.Birds.List.Controller;
});