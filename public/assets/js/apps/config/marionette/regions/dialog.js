define(["marionette", "jquery-ui", "timepicker"], function(Marionette) {
    Marionette.Region.Dialog = Marionette.Region.extend({
        onShow: function(view) {
            this.listenTo(view, "dialog:close", this.closeDialog);
            $('#calendar').datetimepicker({
                maxDate: new Date(),
                minDate: new Date(2007, 6, 12)
            });
            var self = this;
            this.$el.dialog({
                modal: true,
                title: view.options.title,
                width: "auto",
                close: function(e, ui) {
                    self.closeDialog();
                }
            });
        },

        closeDialog: function() {
            this.stopListening();
            this.empty();
            this.$el.dialog("destroy");
        }
    });

    return Marionette.Region.Dialog;
});