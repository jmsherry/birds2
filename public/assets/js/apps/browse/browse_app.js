define(["BirdsApp", "entities/bird"], function(BirdsApp) {
    BirdsApp.module("Browse", function(Browse, BirdsApp, Backbone, Marionette, $, _) {

        Browse.startWithParent = false;

        Browse.onStart = function() {
            console.log("starting Browse");
            if (!localStorage.getItem('birds')) {
                BirdsApp.request("bird:entities");
            }
        };

        Browse.onStop = function() {
            console.log("stopping Browse");
        };
    });
    BirdsApp.module("Routers.Browse", function(BrowseRouter, BirdsApp, Backbone, Marionette, $, _) {
        BrowseRouter.Router = Marionette.AppRouter.extend({
            appRoutes: {
                //"browsep=0": "setupBirds"
                "browse(/:type)(/:subtype)(/:name)?p=:page": "setupBirds"
            }
        });

        var executeAction = function(action, arg) {
            BirdsApp.startSubApp("Browse");
            action(arg);
            BirdsApp.execute("set:active:header", "browse");
        };

        var API = {
            listBirds: function(options) {
                console.log('in listBirds');
                require(["apps/browse/list/list_controller"], function(ListController) {
                    executeAction(ListController.listBirds, options);
                });
            },
            setupBirds: function(options) {
                //console.log('in setupBirds', arguments);
                require(["apps/browse/setup/setup_controller"], function(SetupController) {
                    executeAction(SetupController.setupBirds, options);
                });
            }
        };

        BirdsApp.on("browse:setup", function() {
            //console.log('browse:setup');
            //API.setupBirds();
        }).on("browse:list", function(options) {
            console.log('browse:list options: ', options);
            API.listBirds(options);
        }).on("browse:filter", function(criterion, lastURL) {
            if (criterion) {
                BirdsApp.navigate("browse/filter/criterion:" + criterion + '?p=0');
            } else {
                BirdsApp.navigate(lastURL);
            }
        }).addInitializer(function() {
            new BrowseRouter.Router({
                controller: API
            });
        });
    });

    return BirdsApp.BrowseRouter;
});