define(["BirdsApp", "apps/header/setup/setup_controller", "apps/header/list/list_controller"], function(BirdsApp, SetupController, ListController) {
    BirdsApp.module("HeaderApp", function(HeaderApp, BirdsApp, Backbone, Marionette, $, _) {
        var API = {
            setup: function() {
                SetupController.setup();
            },
            login: function() {
                ListController.showMenu(true);
            },
            logout: function() {
                ListController.showMenu(false);
            },
            deselect: function() {
                SetupController.deselect();
            }
        };

        BirdsApp.commands.setHandler("set:active:header", function(name) {
            SetupController.setActiveHeader(name);
        });

        BirdsApp.on('setup:authenticate', function() {
            //console.log('setup:authenticate');
            //API.setup();
        }).on('login:success', function() {
            console.log('login:success');
            API.login();
        }).on('logout', function() {
            console.log('logout');
            API.logout();
        }).on('brand:clicked', function() {
            console.log('brand:clicked');
            BirdsApp.execute("set:active:header", "browse");
        }).on('menu:deselect', function() {
            console.log('menu:deselect');
            API.deselect();
        });

        HeaderApp.on("start", function() {
            API.setup();
        });
    });

    return BirdsApp.HeaderApp;
});