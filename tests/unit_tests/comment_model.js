var assert = require("assert");
var Backbone = require('backbone');
var comment = {};

beforeEach(function() {
    var Comment = Backbone.Model.extend({

        urlRoot: "data/comments",
        idAttribute: "_id",
        initialize: function() {

        },
        validate: function(attrs, options) {

        }

    });

    comment = new Comment();
    // console.log(bird);
    // return bird;
});

describe('Comment', function() {

    it('should have a urlRoot of \'data/comments\'', function() {
        assert.equal(comment.urlRoot, 'data/comments');
    });

});