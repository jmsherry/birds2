define(["BirdsApp", "apps/profile/setup/setup_view"], function(BirdsApp, View) {
    BirdsApp.module("AuthApp.Profile", function(Profile, BirdsApp, Backbone, Marionette, $, _) {
        Profile.Controller = {
            setup: function() {
                var profile = new View.UserProfile();
            }
        };
    });

    return BirdsApp.AuthApp.Profile.Controller;
});