define([
    'BirdsApp',
    'apps/config/storage/localstorage'
], function(BirdsApp) {
    //console.log('BirdsApp: ', BirdsApp);
    BirdsApp.module("Entities", function(Entities, BirdsApp, Backbone, Marionette, $, _) {

        //The chat module uses socket.io, so any CRUD with the server is handled there...

        Entities.Comment = Backbone.Model.extend({

            urlRoot: "data/comments",
            idAttribute: "_id",
            ajaxSync: Backbone.ajaxSync,
            validate: function(attrs, options) {

            }

        });

        Entities.configureStorage(Entities.Comment);

        Entities.CommentCollection = Backbone.Collection.extend({
            model: Entities.Comment,
            comparator: "name",
            url: "data/comments",
            ajaxSync: Backbone.ajaxSync
        });

        Entities.configureStorage(Entities.CommentCollection);


        var API = {

            getCommentEntities: function() {
                var comments = new Entities.CommentCollection(),
                    defer = $.Deferred(),
                    storedComments = localStorage.getItem('data/comments');

                if (storedComments === null || !BirdsApp.updated.comments && Offline.state === 'up') {

                    //Fetch via Ajax
                    comments.ajaxSync("read", comments, {
                        success: function(data) {
                            _.forEach(data, function(comment) {
                                var thisComment = new Entities.Comment(comment);
                                comments.add(thisComment);
                            });

                            comments.forEach(function(comment) {
                                comment.save();
                            });

                            BirdsApp.updated.comments = true;

                            defer.resolve(comments);
                        },
                        error: function(err) {
                            console.log(err);
                            alert('Error: ' + err.status + ' ' + err.statusText + '\n' + err.responseText + ' \nPlease refresh the page or contact the maintainer');
                        }
                    });

                } else {

                    //Fetch from local Storage
                    comments.fetch({
                        success: function(collection, response, options) {
                            console.log('fetched comments: ', collection.length, arguments);
                            defer.resolve(collection);
                        },
                        error: function(collection, response, options) {
                            console.log('error: ', arguments);
                        }
                    });

                    //console.log('THIS ONE', comments);

                }
                return defer.promise();

            }
        };

        BirdsApp.reqres.setHandler("comment:entities", function() {
            console.log('comment:entities');
            return API.getCommentEntities();
        });

    });

    return;
});