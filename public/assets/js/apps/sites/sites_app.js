define(["BirdsApp"], function(BirdsApp) {
    BirdsApp.module("SitesApp", function(SitesApp, BirdsApp, Backbone, Marionette, $, _) {
        SitesApp.startWithParent = false;

        SitesApp.onStart = function() {
            console.log("starting SitesApp");
        };

        SitesApp.onStop = function() {
            console.log("stopping SitesApp");
        };
    });

    BirdsApp.module("Routers.SitesApp", function(SitesRouter, BirdsApp, Backbone, Marionette, $, _) {
        SitesRouter.Router = Marionette.AppRouter.extend({
            appRoutes: {
                "sites": "list"
            }
        });

        var executeAction = function(action, arg) {
            action(arg);
            BirdsApp.execute("set:active:header", "sites");
        };

        var API = {
            list: function(options) {
                BirdsApp.startSubApp("SitesApp");
                require(["apps/sites/list/list_controller"], function(ListController) {
                    executeAction(ListController.list, options);
                });
            } //,
            // add: function(options) {
            //     require(["./apps/sites/add/add_controller"], function(AddController) {
            //         executeAction(AddController.add, options);
            //     });
            // },
            // edit: function(options) {
            //     require(["./apps/sites/edit/edit_controller"], function(EditController) {
            //         executeAction(EditController.edit, options);
            //     });
            // },
            // remove: function(options) {
            //     require(["./apps/sites/remove/remove_controller"], function(RemoveController) {
            //         executeAction(RemoveController.remove, options);
            //     });
            // }
        };

        BirdsApp.on("sites:list", function() {
            console.log('sites:list');
            //BirdsApp.navigate("/sites");
            API.list();
        }); //.on("sites:add", function(id) {
        //     console.log('sites:add');
        //     //BirdsApp.navigate("/sites/add/");
        //     API.add();
        // }).on("sites:edit", function(id) {
        //     console.log('sites:edit');
        //     //BirdsApp.navigate("/sites/edit/" + id);
        //     API.edit(id);
        // }).on("sites:remove", function(id) {
        //     console.log('sites:remove');
        //     //BirdsApp.navigate("/sites/remove/" + id);
        //     API.remove(id);
        // });


        BirdsApp.addInitializer(function() {
            new SitesRouter.Router({
                controller: API
            });
        });
    });

    return BirdsApp.SitesRouter;
});