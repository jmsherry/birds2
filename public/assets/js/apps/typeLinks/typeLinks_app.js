define(["BirdsApp"], function(BirdsApp) {
    BirdsApp.module("TypeLinksApp", function(TypeLinksApp, BirdsApp, Backbone, Marionette, $, _) {

        TypeLinksApp.startWithParent = false;

        TypeLinksApp.onStart = function() {
            console.log("starting TypeLinksApp");
        };

        TypeLinksApp.onStop = function() {
            console.log("stopping TypeLinksApp");
        };

    });

    BirdsApp.module("Routers.TypeLinksApp", function(TypeLinksApp, BirdsApp, Backbone, Marionette, $, _) {
        TypeLinksApp.Router = Marionette.AppRouter.extend({
            appRoutes: {
                "/": "setupLinks"
            }
        });

        var API = {
            setupLinks: function(criterion) {
                //console.log('TRIGGERED');
                require(["apps/typeLinks/setup/setup_controller"], function(setupController) {
                    setupController.setupLinks(criterion);
                });
            }
        };

        BirdsApp.addInitializer(function() {
            new TypeLinksApp.Router({
                controller: API
            });
        });

        BirdsApp.addInitializer(function() {
            API.setupLinks();
        });
    });

    return BirdsApp.TypeLinksApp;
});