define(["BirdsApp", "backbone-syphon"],
    function(BirdsApp, listTpl, listItemTpl) {
        BirdsApp.module("AuthApp.SignUp.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.Signup = Marionette.ItemView.extend({
                template: 'signup.dust',

                //el: '#signupForm',

                events: {
                    "click button[type=submit]": "signup"
                },

                signup: function() {
                    var user = Backbone.Syphon.serialize(this);
                    console.log(user);
                    BirdsApp.trigger('signup:submit', user);
                    return false;
                }

            });

            View.SignupSetup = Marionette.ItemView.extend({
                el: '#signupForm',
                //$el: $('#signupForm'),

                events: {
                    "click button[type=submit]": "signup"
                },

                signup: function() {
                    var user = Backbone.Syphon.serialize(this);
                    console.log(user);
                    BirdsApp.trigger('signup:submit', user);
                    return false;
                },
                render: function() {
                    this.bindUIElements();
                }
            });

        });
        return BirdsApp.AuthApp.SignUp.View;
    });