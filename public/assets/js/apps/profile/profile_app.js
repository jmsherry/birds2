define(["BirdsApp", "apps/auth/setup/setup_controller", "apps/auth/signup/signup_controller", "apps/auth/authenticate/auth_controller",
        "apps/profile/setup/setup_controller", "apps/profile/list/list_controller"
    ],
    function(BirdsApp, SetupController, SignUpController, AuthController, ProfileSetupController, ProfileListController) {
        console.log(ProfileSetupController);
        BirdsApp.module("ProfileApp", function(ProfileApp, BirdsApp, Backbone, Marionette, $, _) {

            ProfileApp.startWithParent = false;

            ProfileApp.onStart = function() {
                console.log("starting ProfileApp");
            };

            ProfileApp.onStop = function() {
                console.log("stopping ProfileApp");
            };
        });
        BirdsApp.module("ProfileRouter", function(ProfileRouter, BirdsApp, Backbone, Marionette, $, _) {

            ProfileRouter.Router = Marionette.AppRouter.extend({
                appRoutes: {
                    "profile": "setup"
                }
            });

            var executeAction = function(action, arg, arg2) {
                BirdsApp.startSubApp("ProfileApp");
                action(arg, arg2);
                BirdsApp.trigger("menu:delselect");
            };

            var API = {
                setup: function() {
                    executeAction(ProfileSetupController.setup);
                },
                profile: function(user, action) {
                    executeAction(ProfileListController.profile, user, action);
                },
                connect: function(user, action) {
                    executeAction(ProfileListController.profile, user, action);
                },
                unlink: function(user, action) {
                    executeAction(ProfileListController.profile, user, action);
                },
            };

            BirdsApp.on("profile", function(user) {
                BirdsApp.trigger('menu:deselect');
                console.log('profile event. user was set as ', user);
                if (user.user === 'currentUser') {
                    BirdsApp.navigate('/profile');
                    var fetchingCurrentUser = BirdsApp.request("user:currentUser");
                    $.when(fetchingCurrentUser).done(function(currentUser) {
                        console.log('fetchingCurrentUser; ', fetchingCurrentUser);
                        API.profile(currentUser);
                    });
                } else {
                    // OUT OF SCOPE - this is for later when profile views become public
                }

            }).on("connect:local", function(user) {
                API.connect(user, 'local');
            }).on("connect:facebook", function(user) {
                API.connect(user, 'facebook');
            }).on("connect:twitter", function(user) {
                API.connect(user, 'twitter');
            }).on("connect:google", function(user) {
                API.connect(user, 'google');
            }).on("unlink:local", function(user) {
                API.profile(user, 'local');
            }).on("unlink:facebook", function(user) {
                API.profile(user, 'facebook');
            }).on("unlink:twitter", function(user) {
                API.profile(user, 'twitter');
            }).on("unlink:google", function(user) {
                API.profile(user, 'google');
            }).addInitializer(function() {
                new ProfileRouter.Router({
                    controller: API
                });
            });

            ProfileRouter.on("start", function() {
                API.setup();
            });
        });

        return BirdsApp.ProfileRouter;
    });