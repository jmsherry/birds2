define([
        "BirdsApp"
    ],
    function(BirdsApp) {
        // console.log('in browse view', arguments);

        BirdsApp.module("Spots.Setup.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.spots = Backbone.Marionette.CompositeView.extend({

                el: "#sightings-wrapper",

                events: {
                    "click.setup .add-spot": "addSpot",
                    "click.setup .update-spot": "updateSpot",
                    "click.setup .delete-spot": "deleteSpot",
                },

                getID: function(href){
                    console.log(href);
                    var id = href.split('/');
                    id = id[id.length-1];
                    console.log('id:', id);
                    return id;
                },

                addSpot: function() {
                    BirdsApp.trigger("spots:addForm");
                    return false;
                },

                updateSpot: function(e) {
                    console.log(e);
                    var id = this.getID(e.target.attributes.href.value);
                    BirdsApp.trigger("spot:updateForm", id);
                    return false;
                },

                deleteSpot: function(e) {
                    var id = this.getID(e.target.attributes.href.value);
                    BirdsApp.trigger("spot:remove", id, $(e.target).parents('tr'));
                    return false;
                },

                initialize: function() {
                    console.log('view args: ', arguments);
                    this.listenTo(this.collection, "reset", function() {
                        alert('collection reset');
                        // this.appendHtml = function(collectionView, itemView, index) {
                        //     console.log('target: ', arguments);
                        //     collectionView.$el.append(itemView.el);
                        // };
                    });
                    this.listenTo(this.collection, "add", function() {
                        alert('collection add');
                        // this.appendHtml = function(collectionView, itemView, index) {
                        //     console.log('target: ', arguments);
                        //     collectionView.$el.append(itemView.el);
                        // };
                    });
                    this.listenTo(this.collection, "remove", function() {
                        alert('collection remove');
                        // this.appendHtml = function(collectionView, itemView, index) {
                        //     console.log('target: ', arguments);
                        //     collectionView.$el.append(itemView.el);
                        // };
                    });
                    this.listenTo(this.collection, "change", function() {
                        alert('collection change');
                        // this.appendHtml = function(collectionView, itemView, index) {
                        //     console.log('target: ', arguments);
                        //     collectionView.$el.append(itemView.el);
                        // };
                    });
                },

                render: function() {
                    this.bindUIElements();
                }
            });
        });

        return BirdsApp.Spots.Setup.View;
    });