define(["BirdsApp", "apps/spot/setup/setup_view"], function(BirdsApp, View) {
    BirdsApp.module("Spots.Setup", function(Setup, BirdsApp,
        Backbone, Marionette, $, _) {
        Setup.Controller = {
            setup: function() {
                console.log('in spots setup', arguments);

                require(["common/views", "entities/spot", "entities/user"], function(CommonViews) {

                    var fetchingspots = BirdsApp.request("spot:entities");
                    $.when(fetchingspots).done(function(spots) {
                        console.log('spots', spots);
                            var spotsSetupView;

                            spotsSetupView = new View.spots({
                                collection: spots,
                                itemviewOptions: {
                                    user: BirdsApp.currentUser
                                }
                            });

                            console.log('spotsSetupView: ', spotsSetupView);
                            BirdsApp.content.show(spotsSetupView);

                    });

                })
            }
        };
    });

    return BirdsApp.Spots.Setup.Controller;
});