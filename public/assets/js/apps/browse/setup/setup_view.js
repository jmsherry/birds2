define(["BirdsApp", "url"],
    function(BirdsApp, url) {
        var URL = url;
        //console.log(URL);
        BirdsApp.module("Browse.Setup.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            var itemView = Marionette.ItemView;

            function navigate(e) {
                var options = {};
                options.url = e.target.attributes.href.value;
                BirdsApp.trigger('browse:list', options);
                return false;
            }


            View.birdsSetup = itemView.extend({
                el: $('.birdDescription'),

                events: {
                    "click.setup .spot": "spot",
                    "click.setup .fieldNotes a": "navigate"
                },

                getID: function(href){
                    console.log(href, typeof href);
                    // var id = href.split('/');
                    // id = id[id.length-1];
                    // console.log('id', id);
                    // return id;
                },

                spot: function(e) {
                    console.log('spot', e.target.attributes.href);
                    var id = this.getID(e.target.attributes.href);
                    BirdsApp.trigger('spots:add', id);
                    return false;
                },
                navigate: navigate,

                render: function() {
                    this.bindUIElements();
                },
                onClose: function(arg1, arg2) {
                    console.log('closing view.birdsSetup', arguments);
                },
                onDomRefresh: function() {
                    console.log('onDomRefresh view.birdsSetup', arguments);
                }
            });

            View.breadcrumbs = itemView.extend({
                el: $('.breadcrumbs'),
                events: {
                    'click .breadcrumbs a': 'navigate'
                },
                navigate: navigate,
                render: function() {
                    this.bindUIElements();
                }
            });

            View.pagination = itemView.extend({
                el: $('.pagination'),
                events: {
                    'click a': 'navigate'
                },

                itemViewOptions: {},

                initialize: function() {},

                navigate: navigate,

                render: function() {
                    this.bindUIElements();
                }
            });


        });

        return BirdsApp.Browse.Setup.View;
    });