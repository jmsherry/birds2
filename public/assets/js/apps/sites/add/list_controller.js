define(["BirdsApp", "apps/browse/list/list_view"], function(BirdsApp, View) {
    BirdsApp.module("Birds.List", function(List, BirdsApp,
        Backbone, Marionette, $, _) {
        List.Controller = {
            listBirds: function() {

                require(["entities/bird"], function() {

                    var fetchingbirds = BirdsApp.request("bird:entities");

                    //var birdsListLayout = new View.Layout();
                    // //var birdsListPanel = new View.Panel();

                    $.when(fetchingbirds).done(function(birds) {
                        console.log('done: ', birds);
                        birds.sort();
                        var birdsListView = new View.birds({
                            collection: birds
                        });

                        // birdsListLayout.on("show", function() {
                        //     //birdsListLayout.panelRegion.show(birdsListPanel);
                        //     birdsListLayout.birdsRegion.show(birdsListView);
                        // });

                        BirdsApp.content.show(birdsListView);
                    });
                });
            }
        };
    });

    return BirdsApp.Birds.List.Controller;
});