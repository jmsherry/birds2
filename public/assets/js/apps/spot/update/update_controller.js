define(["BirdsApp", "apps/spot/update/update_view"], function(BirdsApp, View) {
    BirdsApp.module("Spots.Update", function(Update, BirdsApp,
        Backbone, Marionette, $, _) {
        Update.Controller = {
            updateForm: function(id) {
                console.log('in spots update', arguments);

                require(["entities/bird", "entities/site"], function() {

                    var fetchingBirds = BirdsApp.request('bird:entities');
                    $.when(fetchingBirds).done(function(birds){

                        var fetchingSites = BirdsApp.request('site:entities');
                        $.when(fetchingSites).done(function(sites){

                            var spotsUpdateView = new View.updateSpot({
                                    birds:birds,
                                    sites:sites,
                                    title: "Update Spot",
                                    spotId:id
                            });
                            console.log('spotsUpdateView: ', spotsUpdateView);

                            BirdsApp.dialogRegion.show(spotsUpdateView);

                        });
                    });

                });
            },
            update: function(vals){
                console.log('in spots update', arguments);

                require(["entities/spot"], function() {

                    var fetchingSpots = BirdsApp.request('spot:entities');
                    $.when(fetchingSpots).done(function(spots){
                        //var newSpot = BirdsApp.Entities.Spot(vals);
                        var oldSpot = spots.get(vals.id);
                        oldSpot.set({
                            "bird": vals.bird,
                            "location": vals.location,
                            "dateTime": vals.dateTime
                        });  
                    });

                });
            }
        };
    });

    return BirdsApp.Spots.Update.Controller;
});