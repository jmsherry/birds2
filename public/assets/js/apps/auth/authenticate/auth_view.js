define(["BirdsApp", "backbone-syphon"],
    function(BirdsApp) {
        BirdsApp.module("AuthApp.List.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.AuthPanel = Marionette.ItemView.extend({

                template: 'authPanel.dust',

                events: {
                    "click .signup a": "signup",
                    "submit form": "login"
                },

                signup: function() {
                    console.log('signing up');
                    BirdsApp.trigger("signup");
                    return false;
                },
                login: function() {
                    var user = Backbone.Syphon.serialize($el.find('form'));
                    console.log(user);
                    BirdsApp.trigger('login', user);
                    return false;
                }
            });

            View.UserMenu = Marionette.ItemView.extend({

                template: 'user_menu.dust',

                events: {
                    "click .profile a": "profile",
                    "click .logout a": "logout"
                },

                logout: function() {
                    console.log('logging out');
                    BirdsApp.trigger("logout");
                    return false;
                },

                profile: function() {
                    console.log('profile');
                    BirdsApp.trigger("profile", {user: 'currentUser'});
                    return false;
                }
            });

        });
        return BirdsApp.AuthApp.List.View;
    });