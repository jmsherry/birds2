define([
        "BirdsApp"
    ],
    function(BirdsApp) {

        BirdsApp.module("TypeLinks.List.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            View.typeLinks = Backbone.Marionette.ItemView.extend({

                events: {
                    "click a": "navigate"
                },
                navigate: function(e) {
                    console.log('triggering typeLink navigation: ', e.currentTarget.attributes.href.value);
                    BirdsApp.trigger('browse:list', {
                        url: e.currentTarget.attributes.href.value
                    });
                    return false;
                },
                render: function() {
                    this.bindUIElements();
                }
            });

        });

        return BirdsApp.TypeLinks.List.View;
    });