define(["BirdsApp", "apps/auth/setup/setup_view"], function(BirdsApp, View) {
    BirdsApp.module("AuthApp.Setup", function(Setup, BirdsApp, Backbone, Marionette, $, _) {
        Setup.Controller = {
            setup: function(user) {
                console.log('IN SETUP CONTROLLER SETUP', user);
                if (user) {
                    var userMenu = new View.UserMenu();
                    var thisUser = new BirdsApp.Entities.User(user);
                    thisUser.set('isCurrentUser', true);
                    thisUser.save();
                    //console.log('setting currentUser', thisUser);
                    BirdsApp.currentUser = thisUser;
                   // console.log('set currentUser', BirdsApp);
                } else {
                    var authPanel = new View.AuthPanel();
                }
            }
        };
    });

    return BirdsApp.AuthApp.Setup.Controller;
});