define(["BirdsApp", "apps/spot/add/add_view", "moment"], function(BirdsApp, View) {
    BirdsApp.module("Spots.Add", function(Add, BirdsApp,
        Backbone, Marionette, $, _) {
        Add.Controller = {
            addForm: function(id) {
                console.log('in spots addForm function', arguments);


                require(["entities/bird", "entities/site"], function() {

                    var fetchingBirds = BirdsApp.request('bird:entities');
                    $.when(fetchingBirds).done(function(birds){

                        var fetchingSites = BirdsApp.request('site:entities');
                        $.when(fetchingSites).done(function(sites){

                            console.log('HERHEHRHERHE', birds, sites);
                            var spotsAddView = new View.addSpot({
                                    birds: birds.models,
                                    sites: sites.models,
                                    title: "Add Spot",
                                    selected: id
                            });
                            console.log('spotsAddView: ', spotsAddView);

                            BirdsApp.dialogRegion.show(spotsAddView);

                        });

                    });

                });
            },
            add: function(vals){
                console.log('in spots add fn', arguments);

                require(["entities/spot"], function() {

                    var fetchingSpots = BirdsApp.request('spot:entities');
                    $.when(fetchingSpots).done(function(spots){
                        //vals.dateTime = moment(vals.dateTime, 'MM/DD/YYYY HH:MM');
                        spots.add(vals);
                    });

                });
            }
        };
    });

    return BirdsApp.Spots.Add.Controller;
});