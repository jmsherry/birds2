define(["BirdsApp", "apps/auth/authenticate/auth_view"], function(BirdsApp, View) {
    BirdsApp.module("AuthApp.Auth", function(Auth, BirdsApp, Backbone, Marionette, $, _) {
        Auth.Controller = {

            login: function(user) {
                console.log('in authenticate login', arguments);
                $.ajax('/async/login', {
                    type: "POST",
                    data: user
                }).success(function(resp) {
                    console.log(arguments);
                    if (resp) {
                        BirdsApp.trigger('login:success', resp);
                    } else {
                        BirdsApp.trigger('login:failure', resp);
                    }
                }).error(function(resp) {
                    console.log(arguments);
                    BirdsApp.trigger('login:failure:error', resp);
                });
            },
            authenticateFrontEnd: function(user, state) {
                console.log('in authenticateFrontEnd', arguments);

                var thisUser = new BirdsApp.Entities.User(user);
                thisUser.set('isCurrentUser', true);
                thisUser.save();
                console.log('setting currentUser', thisUser);
                BirdsApp.currentUser = thisUser;
                console.log('set currentUser', BirdsApp);
                if (state) {
                    this.showUserMenu(thisUser);
                }
            },
            showUserMenu: function(user) {
                //var user = BirdsApp.currentUser;
                var userMenu = new View.UserMenu({
                    user: user
                });
                BirdsApp.auth.show(userMenu);
            },
            showAuthPanel: function(message) {
                var authPanel = new View.AuthPanel({
                    message: message
                });

                BirdsApp.auth.show(authPanel);
            },
            authenticateFailed: function(message) {
                console.log('in authenticateFailed', arguments);
                this.showAuthPanel(message);
            },
            logout: function(user) {
                console.log('in authenticate logout', arguments);
                $.ajax('/async/logout', {
                    data: user
                }).success(function() {
                    var authPanel = new View.AuthPanel({
                        message: 'You have been successfully logged out.'
                    });
                    BirdsApp.auth.show(authPanel);
                    BirdsApp.currentUser = {};

                    BirdsApp.trigger('browse:list');
                });
            },
            confirmAuthenticated: function() {
                console.log('confirming authenticated');
                $.ajax('/authconfirm').success(function(resp) {
                    if (resp) {
                        console.log('was logged in');
                        BirdsApp.trigger('setup:authenticate', resp);
                    } else {
                        console.log('wasn\'t logged in');
                        BirdsApp.trigger('setup:authenticate', null);
                    }
                });
            }

        };
    });

    return BirdsApp.AuthApp.Auth.Controller;
});