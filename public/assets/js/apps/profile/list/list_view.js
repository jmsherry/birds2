define(["BirdsApp", "backbone-syphon"],
    function(BirdsApp) {
        BirdsApp.module("AuthApp.Profile.View", function(View, BirdsApp, Backbone, Marionette, $, _) {
            View.UserProfile = Marionette.ItemView.extend({

                template: 'profile.dust',
                serializeData: function() {
                    var options = {};
                    options.user = this.options.user.attributes;
                    return options;
                }


                // events: {

                // }

            });

        });
        return BirdsApp.AuthApp.Profile.View;
    });