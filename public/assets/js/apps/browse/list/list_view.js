define(["BirdsApp"],
    function(BirdsApp) {

        BirdsApp.module("Browse.List.View", function(View, BirdsApp, Backbone, Marionette, $, _) {

            function navigate(e) {
                var options = {},
                    url, page;
                url = e.target.attributes.href.value;
                //BirdsApp.navigate(url);
                options.url = url;
                options.page = page;
                BirdsApp.trigger('browse:list', options);
                return false;
            }

            View.listLayout = Marionette.LayoutView.extend({
                template: 'listLayout.dust',
                regions: {
                    listRegion: "#list-region",
                    paginationRegion: "#pagination-region"
                },
                id: 'listLayout',
                className: 'listLayout'
            });

            View.breadcrumbs = Marionette.ItemView.extend({
                tagName: "ol",
                template: 'breadcrumbs.dust',
                events: {
                    'click a': 'navigate'
                },
                serializeData: function() {
                    //console.log('in breadcrumbs serializeData: ', this.options);
                    return this.options;
                },
                itemViewOptions: {},
                navigate: navigate,
                initialize: function() {
                    if ($('#breadcrumbs').length === 0) {
                        $('#content').prepend('<div class="breadcrumbs row" id="breadcrumbs"><ol></ol></div>');
                    }
                }
            });



            View.bird = Marionette.ItemView.extend({
                tagName: "li",
                template: 'bird_description.dust',
                className: "col-md-4",

                events: {
                    "click .options a": "spot",
                    "click .fieldNotes a": "navigate"
                },
                spot: function(e) {
                    console.log('spot');
                    var birdId = $('this').data('id');
                    BirdsApp.trigger('spot', birdId);
                    return false;
                },
                initialize: function(options) {
                    console.log(arguments);
                    this.options.itemViewOptions = options;
                },
                serializeData: function() {
                    // console.log('SDARgs: ', arguments);
                    // console.log('name: ', this.model.get('name'));
                    // console.log('in bird serializeData: ', this.options.model.get('name'));
                    var m = this.options.model.attributes;
                    var bird = {
                        "name": m.name,
                        "type": m.type,
                        "subtype": m.subtype,
                        "description": m.description,
                        "fullDescription": m.fullDescription,
                        "length": m.length,
                        "lifespan": m.lifespan,
                        "social": m.social,
                        "status": m.status,
                        "weight": m.weight,
                        "wingspan": m.wingspan,
                        "soloView": this.options.itemViewOptions.soloView
                    };
                    //console.log(bird);
                    // var options = this.options;
                    // return _.extend(bird, options);
                    return bird;
                },
                onRender: function() {
                    //console.log('onRender');
                    if (this.options.itemViewOptions.soloView) {
                        this.$el.addClass('wholePageView');
                    }
                },
                navigate: navigate
            });

            View.noBirds = Marionette.ItemView.extend({
                tagName: 'li',
                template: 'no_results'
            });

            View.birds = Marionette.CompositeView.extend({
                template: 'birds_list_container.dust',
                childView: View.bird,
                emptyView: View.noBirds,
                childViewContainer: "ul",
                childViewOptions: this.options.itemViewOptions,
                serializeData: function() {
                    var vals = this.options.itemViewOptions;
                    var options = {
                        type: vals.type,
                        subtype: vals.subtype,
                        name: vals.name,
                        soloView: vals.soloView,
                        authenticated: true // need a way to pass this through...
                    };
                    this.childViewOptions = options;
                    return options;
                }

            });

            View.pagination = Marionette.ItemView.extend({
                template: 'pagination.dust',
                events: {
                    'click a': 'navigate'
                },
                className: 'pagination',
                id: 'pagination',

                initialize: function(options) {},
                serializeData: function() {
                    console.log('in serializeData: ', this.options);
                    return this.options;
                },

                navigate: navigate
            });
        });

        return BirdsApp.Browse.List.View;
    });