module.exports = function(dustjs) {

    //Middleware
    var flash = require('connect-flash'),
        methodOverride = require('method-override'),
        cookieParser = require('cookie-parser'),
        session = require('cookie-session'),
        bodyParser = require('body-parser'),
        favicon = require('static-favicon'),
        errorHandler = require('error-handler'),
        passport = require('passport'),
        express = require('express'),
        path = require('path'),
        pubDir, sessionVars,
        app;

    app = express();

    app.settings.env = process.env.NODE_ENV;

    // For rendering precompiled templates:
    // app.engine('js', dustjs.js({
    //     cache: false
    // }));
    // app.set('view engine', 'js');
    // app.set('views', path.resolve(__dirname, '../templates/compiled'));

    app.engine('dust', dustjs.dust({
        cache: false
    }));
    app.set('view engine', 'dust');
    app.set('views', path.resolve(__dirname, '../templates'));

    app.use(methodOverride());

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(cookieParser());

    sessionVars = {
        secret: 'BIRDY',
        //secureProxy: true, // if you do SSL outside of node
        maxAge: 2592000000,
        saveUninitialized: false,
        resave: false,
        cookie: {
            secure: false
        }
    };

    app.use(session(sessionVars));

    if ('development' == app.settings.env) {
        pubDir = path.resolve(__dirname, '../public');
        app.use(express.static(pubDir));
    }

    if ('production' == app.settings.env) {
        app.set('trust proxy', 1); // trust first proxy
        sessionVars.cookie.secure = true; // serve secure cookies
        pubDir = path.resolve(__dirname, '../dist');
        app.use(express.static(pubDir));
    }



    app.use(flash());

    require('./../modules/auth/passport')(passport, app);
    app.use(passport.initialize());
    app.use(passport.session());


    dustjs.debugLevel = 'debug';


    app.use(favicon(pubDir + '/assets/images/favicon.ico', {
        maxAge: 2592000000
    }));

    return app;
};