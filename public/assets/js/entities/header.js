define(["BirdsApp", "backbone-picky"], function(BirdsApp, Picky) {
    //console.log(arguments);
    BirdsApp.module("Entities", function(Entities, BirdsApp, Backbone, Marionette, $, _) {

        Entities.Header = Backbone.Model.extend({
            initialize: function() {
                var selectable = new Backbone.Picky.Selectable(this);
                _.extend(this, selectable);
            }
        });

        Entities.HeaderCollection = Backbone.Collection.extend({
            model: Entities.Header,

            initialize: function() {
                var singleSelect = new Backbone.Picky.SingleSelect(this);
                _.extend(this, singleSelect);
            }
        });

        var initializeHeaders = function() {
            Entities.headers = new Entities.HeaderCollection([{
                "name": "browse",
                "isPrivate": false,
                "destinationURL": "/browse?p=0",
                "navigationTrigger": "birds:list"
            }, {
                "name": "spot",
                "isPrivate": true,
                "destinationURL": "/spot",
                "navigationTrigger": "spots:list"
            }, {
                "name": "chatroom",
                "isPrivate": false,
                "destinationURL": "/chatroom",
                "navigationTrigger": "chat:show"
            }, {
                "name": "sites",
                "isPrivate": false,
                "destinationURL": "/sites",
                "navigationTrigger": "sites:list"
            }]);
        };



        var API = {
            getHeaders: function() {
                if (Entities.headers === undefined) {
                    initializeHeaders();
                }
                //console.log('in getHeaders: ', Entities.headers);
                return Entities.headers;
            }
        };

        BirdsApp.reqres.setHandler("header:entities", function() {
            //console.log('requested headers');
            return API.getHeaders();
        });
    });

    return;
});