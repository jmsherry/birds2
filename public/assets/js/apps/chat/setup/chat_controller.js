define(["BirdsApp", "apps/chat/setup/chat_view", "io"], function(BirdsApp, View, io) {
    BirdsApp.module("Chat.Setup", function(Setup, BirdsApp, Backbone, Marionette, $, _) {
        Setup.Controller = {
            setup: function(options) {

                require(["entities/user", "entities/comment"], function(user) {

                    var fetchingusers = BirdsApp.request("user:entities");
                    $.when(fetchingusers).done(function(users) {
                        //console.log('fetchingusers', users);
                        var currentUser, socket, commentsListView, img, saver;

                        currentUser = BirdsApp.currentUser;
                        //console.log('currentUser: ', currentUser);

                        var fetchingComments = BirdsApp.request("comment:entities");
                        $.when(fetchingComments).done(function(comments) {
                           console.log('COMMENTS ', comments);

                            //socket = io.connect('http://localhost:8080/socket');
                            socket = io.connect();

                            //load last chat records whilst waiting for connection
                            commentsListView = new View.comments({
                                collection: comments,
                                itemViewOptions: {
                                    currentUser: currentUser,
                                    users: users,
                                    socket: socket
                                }
                            });

                            commentsListView.on('chat:joined', function(user){
                                //alert('joined');
                                var joinedView = new View.joined({
                                    user:user,
                                    time: moment
                                });
                                this.appendHtml(joinedView);
                            });

                            commentsListView.on('chat:exited', function(user){
                                //alert('exited');
                                var exitedView = new View.exited({
                                    user:user,
                                    time: moment
                                });
                                this.appendHtml(exitedView);
                            });

                            console.log('commentsListView: ', commentsListView);
                            BirdsApp.content.show(commentsListView);


                            socket.on('connect', function(data) {
                                console.log('connected clientside');
                                socket.emit('join', currentUser);
                            });

                            socket.on('conversation', function(conversation) {
                                console.log('received conversation', conversation);
                                var newComments = conversation.comments;
                                console.log('newComments', newComments);
                                if(newComments.length > 0){
                                    comments.reset(newComments);
                                }
                            });

                            socket.on('newUser', function(newUsers, newUser) {
                                console.log('received newUser', newUsers);
                                users.reset(newUsers);
                                commentsListView.trigger('chat:joined', newUser);
                            });

                            socket.on('disconnect', function(status) {
                                if(status === 'transport error'){
                                    return;
                                }
                                commentsListView.trigger('chat:exited', status);
                                console.log('someone disconnected', status);
                            });

                            socket.on('leave', function(status) {
                                if(status === 'transport error'){
                                    return;
                                }
                                commentsListView.trigger('chat:exited', newUser);
                                console.log('someone disconnected', status);
                            });

                        });
                    }); //eo users when
                }); //eo req

            } //eo setup
        }; // controller
    }); //eo module

    return BirdsApp.Chat.Setup.Controller;
});